package main.java.Guis.Items;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import main.java.Recipes.RecipesList;

public class opengui {

	public static Inventory andurilgui = Bukkit.createInventory(null, 54, ChatColor.GREEN + "Anduril");

	public static void openandurilgui(Player p) {

		ItemStack feather = new ItemStack(Material.FEATHER);
		ItemStack br = new ItemStack(Material.BLAZE_ROD);
		ItemStack ib = new ItemStack(Material.IRON_BLOCK);

		andurilgui.setItem(3, feather);
		andurilgui.setItem(4, ib);
		andurilgui.setItem(5, feather);
		andurilgui.setItem(12, feather);
		andurilgui.setItem(13, ib);
		andurilgui.setItem(14, feather);
		andurilgui.setItem(21, feather);
		andurilgui.setItem(22, br);
		andurilgui.setItem(23, feather);
		andurilgui.setItem(40, RecipesList.Anduril());
		andurilgui.setItem(49, RecipesList.back());

		p.openInventory(andurilgui);

	}

	public static Inventory bopgui = Bukkit.createInventory(null, 54, ChatColor.YELLOW + "Book of Protection");

	public static void bopgui(Player p) {

		ItemStack Flint = new ItemStack(Material.FLINT);
		ItemStack paper = new ItemStack(Material.PAPER);
		ItemStack ironingot = new ItemStack(Material.IRON_INGOT);

		bopgui.setItem(22, paper);
		bopgui.setItem(14, paper);
		bopgui.setItem(13, paper);
		bopgui.setItem(23, ironingot);
		bopgui.setItem(40, RecipesList.bop());
		bopgui.setItem(49, RecipesList.back());

		p.openInventory(bopgui);

	}

	public static Inventory bosgui = Bukkit.createInventory(null, 54, ChatColor.YELLOW + "Book of Sharpening");

	public static void bosgui(Player p) {

		ItemStack Flint = new ItemStack(Material.FLINT);
		ItemStack paper = new ItemStack(Material.PAPER);
		ItemStack ironsword = new ItemStack(Material.IRON_SWORD);
		ItemStack bos = RecipesList.bos();

		bosgui.setItem(3, Flint);
		bosgui.setItem(22, paper);
		bosgui.setItem(14, paper);
		bosgui.setItem(13, paper);
		bosgui.setItem(23, ironsword);
		bosgui.setItem(40, bos);
		bosgui.setItem(49, RecipesList.back());

		p.openInventory(bosgui);

	}

	public static Inventory cupidsbowgui = Bukkit.createInventory(null, 54, ChatColor.RED + "Cupid's Bow");

	public static void opencupidsbowgui(Player p) {

		ItemStack blazerod = new ItemStack(Material.BLAZE_ROD);
		ItemStack bow = new ItemStack(Material.BOW);
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		ItemStack lavabucket = new ItemStack(Material.LAVA_BUCKET);
		ItemStack cupidsbow = RecipesList.CupidsBow();

		cupidsbowgui.setItem(4, blazerod);
		cupidsbowgui.setItem(22, lavabucket);
		cupidsbowgui.setItem(12, head);
		cupidsbowgui.setItem(14, head);
		cupidsbowgui.setItem(13, bow);
		cupidsbowgui.setItem(40, cupidsbow);
		cupidsbowgui.setItem(49, RecipesList.back());

		p.openInventory(cupidsbowgui);

	}

	public static Inventory draggui = Bukkit.createInventory(null, 54, ChatColor.GREEN + "Dragon Sword");

	public static void opendraggui(Player p) {

		ItemStack magma = new ItemStack(Material.MAGMA_CREAM);
		ItemStack diasword = new ItemStack(Material.DIAMOND_SWORD);
		ItemStack obby = new ItemStack(Material.OBSIDIAN);
		ItemStack drag = RecipesList.DragonSword();

		draggui.setItem(4, magma);
		draggui.setItem(22, magma);
		draggui.setItem(21, obby);
		draggui.setItem(23, obby);
		draggui.setItem(13, diasword);
		draggui.setItem(40, drag);
		draggui.setItem(49, RecipesList.back());

		p.openInventory(draggui);

	}

	public static Inventory gheadgui = Bukkit.createInventory(null, 54, ChatColor.GOLD + "Golden Head");

	public static void opengheadgui(Player p) {

		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		ItemStack gold = new ItemStack(Material.GOLD_INGOT);
		ItemStack ghead = RecipesList.ghead();

		gheadgui.setItem(3, gold);
		gheadgui.setItem(4, gold);
		gheadgui.setItem(5, gold);
		gheadgui.setItem(12, gold);
		gheadgui.setItem(13, head);
		gheadgui.setItem(14, gold);
		gheadgui.setItem(21, gold);
		gheadgui.setItem(22, gold);
		gheadgui.setItem(23, gold);
		gheadgui.setItem(40, ghead);
		gheadgui.setItem(49, RecipesList.back());

		p.openInventory(gheadgui);

	}

	public static Inventory quickpickgui = Bukkit.createInventory(null, 54, RecipesList.QuickPick().getItemMeta().getDisplayName());

	public static void openquickpickgui(Player p) {

		ItemStack item1 = new ItemStack(Material.IRON_ORE);
		ItemStack item2 = new ItemStack(Material.COAL);
		ItemStack item3 = new ItemStack(Material.STICK);

		quickpickgui.setItem(3, item1);
		quickpickgui.setItem(4, item1);
		quickpickgui.setItem(5, item1);
		quickpickgui.setItem(12, item2);
		quickpickgui.setItem(13, item3);
		quickpickgui.setItem(14, item2);
		quickpickgui.setItem(22, item3);
		quickpickgui.setItem(40, RecipesList.QuickPick());
		quickpickgui.setItem(49, RecipesList.back());

		p.openInventory(quickpickgui);

	}

	public static Inventory vorpalgui = Bukkit.createInventory(null, 54, ChatColor.GREEN + "Vorpal Sword");

	public static void VorpalGUI(Player p) {

		ItemStack material1 = new ItemStack(Material.BONE);
		ItemStack material2 = new ItemStack(Material.IRON_SWORD);
		ItemStack material3 = new ItemStack(Material.ROTTEN_FLESH);
		ItemStack vorpalSword = RecipesList.VorpalSword();

		vorpalgui.setItem(4, material1);
		vorpalgui.setItem(22, material3);
		vorpalgui.setItem(13, material2);
		vorpalgui.setItem(40, vorpalSword);
		vorpalgui.setItem(49, RecipesList.back());

		p.openInventory(vorpalgui);

	}

	public static Inventory sevenleaguegui = Bukkit.createInventory(null, 54, ChatColor.GREEN + "Seven League Boots");

	public static void opensevenleaguegui(Player p) {

		ItemStack feather = new ItemStack(Material.FEATHER);
		ItemStack item2 = new ItemStack(Material.ENDER_PEARL);
		ItemStack item3 = new ItemStack(Material.DIAMOND_BOOTS);
		ItemStack item4 = new ItemStack(Material.WATER_BUCKET);

		sevenleaguegui.setItem(3, feather);
		sevenleaguegui.setItem(4, item2);
		sevenleaguegui.setItem(5, feather);
		sevenleaguegui.setItem(12, feather);
		sevenleaguegui.setItem(13, item3);
		sevenleaguegui.setItem(14, feather);
		sevenleaguegui.setItem(21, feather);
		sevenleaguegui.setItem(22, item4);
		sevenleaguegui.setItem(23, feather);
		sevenleaguegui.setItem(40, RecipesList.SevenLeagueBoots());
		sevenleaguegui.setItem(49, RecipesList.back());

		p.openInventory(sevenleaguegui);

	}

	public static Inventory ironpackgui = Bukkit.createInventory(null, 54, ChatColor.GRAY + "Iron Pack");

	public static void openironpackgui(Player p) {

		ItemStack coal = new ItemStack(Material.COAL);
		ItemStack iron = new ItemStack(Material.IRON_ORE);
		ItemStack ironpack = RecipesList.IronPack();

		ironpackgui.setItem(3, iron);
		ironpackgui.setItem(4, iron);
		ironpackgui.setItem(5, iron);
		ironpackgui.setItem(12, iron);
		ironpackgui.setItem(13, coal);
		ironpackgui.setItem(14, iron);
		ironpackgui.setItem(21, iron);
		ironpackgui.setItem(22, iron);
		ironpackgui.setItem(23, iron);
		ironpackgui.setItem(40, ironpack);
		ironpackgui.setItem(49, RecipesList.back());

		p.openInventory(ironpackgui);

	}

	public static Inventory dragchestgui = Bukkit.createInventory(null, 54, ChatColor.GREEN + "Dragon Chestplate");

	public static void opendragchestgui(Player p) {

		ItemStack magma = new ItemStack(Material.MAGMA_CREAM);
		ItemStack diachest = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemStack obby = new ItemStack(Material.OBSIDIAN);
		ItemStack anvil = new ItemStack(Material.ANVIL);
		ItemStack dragchest = RecipesList.dragonchestplate();

		dragchestgui.setItem(4, magma);
		dragchestgui.setItem(22, anvil);
		dragchestgui.setItem(21, obby);
		dragchestgui.setItem(23, obby);
		dragchestgui.setItem(13, diachest);
		dragchestgui.setItem(40, dragchest);
		dragchestgui.setItem(49, RecipesList.back());

		p.openInventory(dragchestgui);

	}

	public static Inventory fusiongui = Bukkit.createInventory(null, 54,
			ChatColor.RED + "Fusion Piece (Shapeless Craft)");

	public static void openfusiongui(Player p) {

		ItemStack item1 = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemStack item2 = new ItemStack(Material.DIAMOND_HELMET);
		ItemStack item3 = new ItemStack(Material.DIAMOND_BOOTS);
		ItemStack item4 = new ItemStack(Material.DIAMOND_LEGGINGS);

		fusiongui.setItem(3, item2);
		fusiongui.setItem(4, item1);
		fusiongui.setItem(5, item4);
		fusiongui.setItem(12, item3);
		fusiongui.setItem(49, RecipesList.back());

		fusiongui.setItem(40, RecipesList.fusioncraftitem());

		p.openInventory(fusiongui);

	}

	public static Inventory goldpackgui = Bukkit.createInventory(null, 54, ChatColor.GRAY + "Gold Pack");

	public static void opengoldpackgui(Player p) {

		ItemStack coal = new ItemStack(Material.COAL);
		ItemStack gold = new ItemStack(Material.GOLD_ORE);
		ItemStack goldpack = RecipesList.goldpack();

		goldpackgui.setItem(3, gold);
		goldpackgui.setItem(4, gold);
		goldpackgui.setItem(5, gold);
		goldpackgui.setItem(12, gold);
		goldpackgui.setItem(13, coal);
		goldpackgui.setItem(14, gold);
		goldpackgui.setItem(21, gold);
		goldpackgui.setItem(22, gold);
		goldpackgui.setItem(23, gold);
		goldpackgui.setItem(40, goldpack);
		goldpackgui.setItem(49, RecipesList.back());

		p.openInventory(goldpackgui);

	}

	public static Inventory philosgui = Bukkit.createInventory(null, 54, RecipesList.philos().getItemMeta().getDisplayName());

	public static void openphilosgui(Player p) {

		ItemStack item1 = new ItemStack(Material.IRON_ORE);
		ItemStack item2 = new ItemStack(Material.LAPIS_BLOCK);
		ItemStack item3 = new ItemStack(Material.STICK);
		ItemStack item4 = new ItemStack(Material.GOLD_ORE);

		philosgui.setItem(3, item1);
		philosgui.setItem(4, item4);
		philosgui.setItem(5, item1);
		philosgui.setItem(12, item2);
		philosgui.setItem(13, item3);
		philosgui.setItem(14, item2);
		philosgui.setItem(22, item3);
		philosgui.setItem(40, RecipesList.philos());
		philosgui.setItem(49, RecipesList.back());

		p.openInventory(philosgui);

	}

	public static Inventory exodusgui = Bukkit.createInventory(null, 54, ChatColor.GRAY + "Gold Pack");

	public static void openexodusgui(Player p) {

		ItemStack item1 = new ItemStack(Material.DIAMOND);
		ItemStack item2 = RecipesList.heads();
		ItemStack item3 = new ItemStack(Material.GOLDEN_CARROT);
		ItemStack item4 = new ItemStack(Material.EMERALD);
		ItemStack item5 = RecipesList.exodus();

		exodusgui.setItem(3, item1);
		exodusgui.setItem(4, item1);
		exodusgui.setItem(5, item1);
		exodusgui.setItem(12, item1);
		exodusgui.setItem(13, item2);
		exodusgui.setItem(14, item1);
		exodusgui.setItem(21, item4);
		exodusgui.setItem(22, item3);
		exodusgui.setItem(23, item4);
		exodusgui.setItem(40, item5);
		exodusgui.setItem(49, RecipesList.back());

		p.openInventory(exodusgui);

	}

	public static Inventory saddlegui = Bukkit.createInventory(null, 54, ChatColor.GRAY + "Saddle");

	public static void opensaddlegui(Player p) {

		ItemStack item1 = new ItemStack(Material.LEATHER);
		ItemStack item3 = new ItemStack(Material.STRING);
		ItemStack item4 = new ItemStack(Material.IRON_INGOT);
		ItemStack item5 = RecipesList.saddle();

		saddlegui.setItem(3, item1);
		saddlegui.setItem(4, item1);
		saddlegui.setItem(5, item1);
		saddlegui.setItem(12, item1);
		saddlegui.setItem(13, item3);
		saddlegui.setItem(14, item1);
		saddlegui.setItem(21, item4);

		saddlegui.setItem(23, item4);
		saddlegui.setItem(40, item5);
		saddlegui.setItem(49, RecipesList.back());

		p.openInventory(saddlegui);

	}

	public static Inventory tarnhelmgui = Bukkit.createInventory(null, 54, RecipesList.tarnhelm().getItemMeta().getDisplayName());

	public static void opentarnhelmgui(Player p) {

		ItemStack item1 = new ItemStack(Material.DIAMOND);
		ItemStack item3 = new ItemStack(Material.REDSTONE_BLOCK);
		ItemStack item4 = new ItemStack(Material.IRON_INGOT);
		ItemStack item5 = RecipesList.tarnhelm();

		tarnhelmgui.setItem(3, item1);
		tarnhelmgui.setItem(4, item4);
		tarnhelmgui.setItem(5, item1);
		tarnhelmgui.setItem(12, item1);
		tarnhelmgui.setItem(13, item3);
		tarnhelmgui.setItem(14, item1);
		tarnhelmgui.setItem(40, item5);
		tarnhelmgui.setItem(49, RecipesList.back());

		p.openInventory(tarnhelmgui);

	}

	public static Inventory tabletofdestinygui = Bukkit.createInventory(null, 54, RecipesList.tabletofdestiny().getItemMeta().getDisplayName());

	public static void opentabletofdestinygui(Player p) {

		ItemStack item1 = new ItemStack(Material.MAGMA_CREAM);
		ItemStack item2 = new ItemStack(Material.BOW);
		ItemStack item3 = new ItemStack(Material.DIAMOND_SWORD);
		ItemStack item4 = new ItemStack(Material.BOOK_AND_QUILL);
		ItemStack item6 = new ItemStack(Material.EXP_BOTTLE);
		ItemStack item5 = RecipesList.tabletofdestiny();

		tabletofdestinygui.setItem(4, item1);

		tabletofdestinygui.setItem(12, item3);
		tabletofdestinygui.setItem(13, item4);
		tabletofdestinygui.setItem(14, item2);
		tabletofdestinygui.setItem(21, item6);
		tabletofdestinygui.setItem(22, item6);
		tabletofdestinygui.setItem(23, item6);
		tabletofdestinygui.setItem(40, item5);
		tabletofdestinygui.setItem(49, RecipesList.back());

		p.openInventory(tabletofdestinygui);

	}

	public static Inventory spikedarmorgui = Bukkit.createInventory(null, 54, RecipesList.spikedarmor().getItemMeta().getDisplayName());

	public static void openspikedarmorgui(Player p) {

		ItemStack item1 = new ItemStack(Material.WATER_LILY);
		ItemStack item2 = new ItemStack(Material.CACTUS);
		ItemStack item3 = new ItemStack(Material.LEATHER_CHESTPLATE);
		ItemStack item5 = RecipesList.spikedarmor();

		spikedarmorgui.setItem(4, item1);

		spikedarmorgui.setItem(13, item2);

		spikedarmorgui.setItem(22, item3);

		spikedarmorgui.setItem(40, item5);
		spikedarmorgui.setItem(49, RecipesList.back());

		p.openInventory(spikedarmorgui);

	}

	public static Inventory axeofperungui = Bukkit.createInventory(null, 54, RecipesList.axeofperun().getItemMeta().getDisplayName());

	public static void openaxeofperungui(Player p) {

		ItemStack item1 = new ItemStack(Material.DIAMOND);
		ItemStack item2 = new ItemStack(Material.TNT);
		ItemStack item3 = new ItemStack(Material.FIREBALL);
		ItemStack item4 = new ItemStack(Material.STICK);
		ItemStack item5 = RecipesList.axeofperun();

		axeofperungui.setItem(3, item1);
		axeofperungui.setItem(4, item2);
		axeofperungui.setItem(5, item3);
		axeofperungui.setItem(12, item1);
		axeofperungui.setItem(13, item4);

		axeofperungui.setItem(22, item4);

		axeofperungui.setItem(40, item5);
		axeofperungui.setItem(49, RecipesList.back());

		p.openInventory(axeofperungui);

	}
	
	public static Inventory Vidargui = Bukkit.createInventory(null, 54, RecipesList.vidar().getItemMeta().getDisplayName());

	public static void openVidargui(Player p) {

		ItemStack item1 = new ItemStack(Material.POTION);
		ItemStack puff = new ItemStack(Material.RAW_FISH, 1, (short) 3);		
		ItemStack item4 = new ItemStack(Material.DIAMOND_BOOTS);
		ItemStack item5 = new ItemStack(Material.FISHING_ROD);

		
		Vidargui.setItem(4, puff);
		
		Vidargui.setItem(12, item1);
		Vidargui.setItem(13, item4);
		Vidargui.setItem(14, item1);
		Vidargui.setItem(22, item5);
		
		
		Vidargui.setItem(40, RecipesList.vidar());
		Vidargui.setItem(49, RecipesList.back());

		p.openInventory(Vidargui);

	}
	
	public static Inventory artemisbookgui = Bukkit.createInventory(null, 54, RecipesList.artbook().getItemMeta().getDisplayName());

	public static void artemisbookgui(Player p) {

		ItemStack paper = new ItemStack(Material.PAPER);
		ItemStack arrow = new ItemStack(Material.ARROW);

		artemisbookgui.setItem(22, paper);
		artemisbookgui.setItem(14, paper);
		artemisbookgui.setItem(13, paper);
		artemisbookgui.setItem(23, arrow);
		artemisbookgui.setItem(40, RecipesList.artbook());
		artemisbookgui.setItem(49, RecipesList.back());

		p.openInventory(artemisbookgui);

	}
	
	public static Inventory bookofthothgui = Bukkit.createInventory(null, 54, RecipesList.bookofthoth().getItemMeta().getDisplayName());

	public static void openbookofthothgui(Player p) {

		ItemStack item1 = new ItemStack(Material.EYE_OF_ENDER);
		ItemStack item2 = new ItemStack(Material.PAPER);
		ItemStack item3 = new ItemStack(Material.FIREBALL);

		tarnhelmgui.setItem(3, item1);
		bookofthothgui.setItem(14, item2);
		bookofthothgui.setItem(13, item2);
		bookofthothgui.setItem(22, item2);
		bookofthothgui.setItem(23, item3);
		bookofthothgui.setItem(40, RecipesList.bookofthoth());
		bookofthothgui.setItem(49, RecipesList.back());

		p.openInventory(bookofthothgui);

	}
	
	public static Inventory fenrirgui = Bukkit.createInventory(null, 54, RecipesList.fenrir().getItemMeta().getDisplayName());

	public static void openfenrirgui(Player p) {

		ItemStack item1 = new ItemStack(Material.LEATHER);
		ItemStack item2 = new ItemStack(Material.BONE);
		ItemStack item3 = new ItemStack(Material.EXP_BOTTLE);
		ItemStack item4 = RecipesList.fenrir();

		fenrirgui.setItem(3, item1);
		fenrirgui.setItem(4, item1);
		fenrirgui.setItem(5, item1);
		fenrirgui.setItem(12, item2);
		fenrirgui.setItem(13, item3);
		fenrirgui.setItem(14, item2);
		fenrirgui.setItem(21, item1);
		fenrirgui.setItem(22, item1);
		fenrirgui.setItem(23, item1);
		fenrirgui.setItem(40, item4);
		fenrirgui.setItem(49, RecipesList.back());

		p.openInventory(fenrirgui);

	}
	
	public static Inventory hideoflgui = Bukkit.createInventory(null, 54, RecipesList.hideofl().getItemMeta().getDisplayName());

	public static void openhideoflgui(Player p) {

		ItemStack item1 = new ItemStack(Material.LAPIS_BLOCK);
		ItemStack item2 = new ItemStack(Material.WATER_BUCKET);
		ItemStack item3 = new ItemStack(Material.DIAMOND_LEGGINGS);
		ItemStack item4 = new ItemStack(Material.DIAMOND);
		ItemStack item5 = new ItemStack(Material.WATER_LILY);
		ItemStack item6 = RecipesList.hideofl();

		hideoflgui.setItem(3, item1);
		hideoflgui.setItem(4, item2);
		hideoflgui.setItem(5, item1);
		hideoflgui.setItem(12, item4);
		hideoflgui.setItem(13, item3);
		hideoflgui.setItem(14, item4);
		hideoflgui.setItem(21, item5);
		
		hideoflgui.setItem(23, item5);
		hideoflgui.setItem(40, item6);
		hideoflgui.setItem(49, RecipesList.back());

		p.openInventory(hideoflgui);

	}
	
	public static Inventory nectargui = Bukkit.createInventory(null, 54, RecipesList.nectar().getItemMeta().getDisplayName());

	public static void opennectargui(Player p) {

		ItemStack item1 = new ItemStack(Material.EMERALD);
		ItemStack item2 = new ItemStack(Material.GOLD_INGOT);
		ItemStack item3 = new ItemStack(Material.MELON);
		ItemStack item4 = new ItemStack(Material.GLASS_BOTTLE);
		ItemStack item6 = RecipesList.nectar();

		nectargui.setItem(4, item1);

		nectargui.setItem(12, item2);
		nectargui.setItem(13, item3);
		nectargui.setItem(14, item2);
		nectargui.setItem(22, item4);
		
		
		nectargui.setItem(40, item6);
		nectargui.setItem(49, RecipesList.back());

		p.openInventory(nectargui);

	}
	
	public static Inventory potionoftoughnessgui = Bukkit.createInventory(null, 54, RecipesList.potionoftoughness().getItemMeta().getDisplayName());

	public static void openpotionoftoughnessgui(Player p) {

		ItemStack item1 = new ItemStack(Material.SLIME_BALL);
		ItemStack item2 = new ItemStack(Material.IRON_BLOCK);
		ItemStack item4 = new ItemStack(Material.GLASS_BOTTLE);
		ItemStack item6 = RecipesList.potionoftoughness();

		potionoftoughnessgui.setItem(4, item1);


		potionoftoughnessgui.setItem(13, item2);
		
		potionoftoughnessgui.setItem(22, item4);
		
		
		potionoftoughnessgui.setItem(40, item6);
		potionoftoughnessgui.setItem(49, RecipesList.back());

		p.openInventory(potionoftoughnessgui);

	}
	
	public static Inventory holywatergui = Bukkit.createInventory(null, 54, RecipesList.holywater().getItemMeta().getDisplayName());

	public static void openholywatergui(Player p) {

		ItemStack item1 = new ItemStack(Material.GOLD_INGOT);
		ItemStack item2 = new ItemStack(Material.REDSTONE_BLOCK);
		ItemStack item3 = new ItemStack(Material.GOLD_RECORD);
		ItemStack item4 = new ItemStack(Material.GLASS_BOTTLE);
		ItemStack item6 = RecipesList.holywater();

		holywatergui.setItem(3, item1);
		holywatergui.setItem(4, item2);
		holywatergui.setItem(5, item1);
		
		holywatergui.setItem(13, item3);
		
		holywatergui.setItem(22, item4);
		
	
		holywatergui.setItem(40, item6);
		holywatergui.setItem(49, RecipesList.back());

		p.openInventory(holywatergui);

	}
	
	public static Inventory lightapplegui = Bukkit.createInventory(null, 54, RecipesList.lightapple().getItemMeta().getDisplayName());

	public static void openlightapplegui(Player p) {

		ItemStack item1 = new ItemStack(Material.GOLD_INGOT);
		ItemStack item2 = new ItemStack(Material.APPLE);
		ItemStack item4 = RecipesList.lightapple();

		
		lightapplegui.setItem(4, item1);
		
		lightapplegui.setItem(12, item1);
		lightapplegui.setItem(13, item2);
		lightapplegui.setItem(14, item1);
		
		lightapplegui.setItem(22, item1);
		
		lightapplegui.setItem(40, item4);
		lightapplegui.setItem(49, RecipesList.back2());

		p.openInventory(lightapplegui);

	}

	public static Inventory artemisgui = Bukkit.createInventory(null, 54, RecipesList.artmeis().getItemMeta().getDisplayName());

	public static void openartemisgui(Player p) {

		ItemStack item1 = new ItemStack(Material.FEATHER);
		ItemStack item2 = new ItemStack(Material.DIAMOND);
		ItemStack item3 = new ItemStack(Material.BOW);
		ItemStack item4 = new ItemStack(Material.EYE_OF_ENDER);
		ItemStack item5 = RecipesList.artmeis();

		artemisgui.setItem(3, item1);
		artemisgui.setItem(4, item2);
		artemisgui.setItem(5, item1);
		artemisgui.setItem(12, item1);
		artemisgui.setItem(13, item3);
		artemisgui.setItem(14, item1);
		artemisgui.setItem(21, item1);
		artemisgui.setItem(22, item4);
		artemisgui.setItem(23, item1);
		artemisgui.setItem(40, item5);
		artemisgui.setItem(49, RecipesList.back2());

		p.openInventory(artemisgui);

	}
	
	public static Inventory pboxgui = Bukkit.createInventory(null, 54, RecipesList.pandorasbox().getItemMeta().getDisplayName());

	public static void openpboxgui(Player p) {

		ItemStack item1 = new ItemStack(Material.WOOD);
		ItemStack item2 = RecipesList.heads();
		ItemStack item5 = RecipesList.pandorasbox();

		pboxgui.setItem(3, item1);
		pboxgui.setItem(4, item1);
		pboxgui.setItem(5, item1);
		pboxgui.setItem(12, item1);
		pboxgui.setItem(13, item2);
		pboxgui.setItem(14, item1);
		pboxgui.setItem(21, item1);
		pboxgui.setItem(22, item1);
		pboxgui.setItem(23, item1);
		pboxgui.setItem(40, item5);
		pboxgui.setItem(49, RecipesList.back2());

		p.openInventory(pboxgui);

	}
	
	public static Inventory deathsythegui = Bukkit.createInventory(null, 54, RecipesList.deathsythe().getItemMeta().getDisplayName());

	public static void opendeathsythegui(Player p) {

		ItemStack item1 = new ItemStack(Material.WOOD);
		ItemStack item2 = RecipesList.heads();
		ItemStack item3 = new ItemStack(Material.BONE);
		ItemStack item4 = new ItemStack(Material.WATCH);
		ItemStack item5 = RecipesList.deathsythe();

		deathsythegui.setItem(4, item2);
		deathsythegui.setItem(5, item2);
		
		
		deathsythegui.setItem(14, item3);
		deathsythegui.setItem(21, item3);

		deathsythegui.setItem(40, item5);
		deathsythegui.setItem(49, RecipesList.back2());

		p.openInventory(deathsythegui);

	}
	
	
	public static Inventory sugarrushgui = Bukkit.createInventory(null, 54, RecipesList.sugarrush().getItemMeta().getDisplayName());

	public static void opensugarrushgui(Player p) {

		ItemStack item1 = new ItemStack(Material.SAPLING);
		ItemStack item2 = new ItemStack(Material.SEEDS); 
		ItemStack item3 = new ItemStack(Material.SUGAR); 
		ItemStack item5 = RecipesList.sugarrush();

	
		sugarrushgui.setItem(4, item1);

		sugarrushgui.setItem(12, item2);
		sugarrushgui.setItem(13, item3);
		sugarrushgui.setItem(14, item2);

		sugarrushgui.setItem(40, item5);
		sugarrushgui.setItem(49, RecipesList.back2());

		p.openInventory(sugarrushgui);

	}
	
	public static Inventory arrowecogui = Bukkit.createInventory(null, 54, RecipesList.arroweco().getItemMeta().getDisplayName());

	public static void openarrowecogui(Player p) {

		ItemStack item1 = new ItemStack(Material.FLINT);
		ItemStack item2 = new ItemStack(Material.STICK);
		ItemStack item3 = new ItemStack(Material.FEATHER);
		ItemStack item5 = RecipesList.arroweco();

		arrowecogui.setItem(3, item1);
		arrowecogui.setItem(4, item1);
		arrowecogui.setItem(5, item1);
		arrowecogui.setItem(12, item2);
		arrowecogui.setItem(13, item2);
		arrowecogui.setItem(14, item2);
		arrowecogui.setItem(21, item3);
		arrowecogui.setItem(22, item3);
		arrowecogui.setItem(23, item3);
		arrowecogui.setItem(40, item5);
		arrowecogui.setItem(49, RecipesList.back2());

		p.openInventory(arrowecogui);

	}
	
	public static Inventory lightanvilgui = Bukkit.createInventory(null, 54, RecipesList.lightanvil().getItemMeta().getDisplayName());

	public static void openlightanvilgui(Player p) {

		ItemStack item1 = new ItemStack(Material.IRON_INGOT);
		ItemStack item2 = new ItemStack(Material.IRON_BLOCK);
		ItemStack item3 = new ItemStack(Material.FEATHER);
		ItemStack item5 = RecipesList.lightanvil();

		lightanvilgui.setItem(3, item1);
		lightanvilgui.setItem(4, item1);
		lightanvilgui.setItem(5, item1);
		
		lightanvilgui.setItem(13, item2);
		
		lightanvilgui.setItem(21, item1);
		lightanvilgui.setItem(22, item1);
		lightanvilgui.setItem(23, item1);
		lightanvilgui.setItem(40, item5);
		lightanvilgui.setItem(49, RecipesList.back2());

		p.openInventory(lightanvilgui);

	}
	
	public static Inventory dustoflightgui = Bukkit.createInventory(null, 54, RecipesList.dustoflight().getItemMeta().getDisplayName());

	public static void opendustoflightgui(Player p) {

		ItemStack item1 = new ItemStack(Material.REDSTONE);
		ItemStack item2 = new ItemStack(Material.FLINT_AND_STEEL);
		ItemStack item5 = RecipesList.dustoflight();

		dustoflightgui.setItem(3, item1);
		dustoflightgui.setItem(4, item1);
		dustoflightgui.setItem(5, item1);
		dustoflightgui.setItem(12, item1);
		dustoflightgui.setItem(13, item2);
		dustoflightgui.setItem(14, item1);
		dustoflightgui.setItem(21, item1);
		dustoflightgui.setItem(22, item1);
		dustoflightgui.setItem(23, item1);
		dustoflightgui.setItem(40, item5);
		dustoflightgui.setItem(49, RecipesList.back2());

		p.openInventory(dustoflightgui);

	}
	
	public static Inventory brewingartifactgui = Bukkit.createInventory(null, 54, RecipesList.brewingartifact().getItemMeta().getDisplayName());

	public static void openbrewingartifactgui(Player p) {

		ItemStack item1 = new ItemStack(Material.SEEDS);
		ItemStack item2 = new ItemStack(Material.FERMENTED_SPIDER_EYE);
		ItemStack item5 = RecipesList.brewingartifact();

		
		brewingartifactgui.setItem(4, item1);
		
		brewingartifactgui.setItem(12, item1);
		brewingartifactgui.setItem(13, item2);
		brewingartifactgui.setItem(14, item1);

		brewingartifactgui.setItem(22, item1);

		brewingartifactgui.setItem(40, item5);
		brewingartifactgui.setItem(49, RecipesList.back2());

		p.openInventory(brewingartifactgui);

	}
	
	public static Inventory netherartifactgui = Bukkit.createInventory(null, 54, RecipesList.netherartifact().getItemMeta().getDisplayName());

	public static void opennetherartifactgui(Player p) {

		ItemStack item1 = new ItemStack(Material.STAINED_GLASS, 1, (short) 1);
		ItemStack item2 = new ItemStack(Material.LAVA_BUCKET);
		ItemStack item3 = new ItemStack(Material.FIREWORK);
		ItemStack item5 = RecipesList.netherartifact();

		netherartifactgui.setItem(3, item1);
		netherartifactgui.setItem(4, item2);
		netherartifactgui.setItem(5, item1);
		netherartifactgui.setItem(12, item1);
		netherartifactgui.setItem(13, item3);
		netherartifactgui.setItem(14, item1);
		netherartifactgui.setItem(21, item1);
		netherartifactgui.setItem(22, item2);
		netherartifactgui.setItem(23, item1);
		netherartifactgui.setItem(40, item5);
		netherartifactgui.setItem(49, RecipesList.back2());

		p.openInventory(netherartifactgui);

	}
	
	public static Inventory deliciousmealgui = Bukkit.createInventory(null, 54, RecipesList.deliciousmeal().getItemMeta().getDisplayName());

	public static void opendeliciousmealgui(Player p) {

		ItemStack item1 = new ItemStack(Material.RAW_BEEF);
		ItemStack item2 = new ItemStack(Material.COAL);
		ItemStack item5 = RecipesList.deliciousmeal();

		deliciousmealgui.setItem(3, item1);
		deliciousmealgui.setItem(4, item1);
		deliciousmealgui.setItem(5, item1);
		deliciousmealgui.setItem(12, item1);
		deliciousmealgui.setItem(13, item2);
		deliciousmealgui.setItem(14, item1);
		deliciousmealgui.setItem(21, item1);
		deliciousmealgui.setItem(22, item1);
		deliciousmealgui.setItem(23, item1);
		deliciousmealgui.setItem(40, item5);
		deliciousmealgui.setItem(49, RecipesList.back2());

		p.openInventory(deliciousmealgui);

	}
	
	public static Inventory evestemptationgui = Bukkit.createInventory(null, 54, RecipesList.evestemptation().getItemMeta().getDisplayName());

	public static void openevestemptationgui(Player p) {

		ItemStack item1 = new ItemStack(Material.APPLE);
		ItemStack item2 = new ItemStack(Material.INK_SACK, 1, (short)15);
		ItemStack item5 = RecipesList.evestemptation();


		evestemptationgui.setItem(13, item2);

		evestemptationgui.setItem(22, item1);
		evestemptationgui.setItem(40, item5);
		evestemptationgui.setItem(49, RecipesList.back2());

		p.openInventory(evestemptationgui);

	}
	
	public static Inventory healingfruitgui = Bukkit.createInventory(null, 54, RecipesList.healingfruit().getItemMeta().getDisplayName());

	public static void openhealingfruitgui(Player p) {

		ItemStack item1 = new ItemStack(Material.APPLE);
		ItemStack item2 = new ItemStack(Material.SEEDS);
		ItemStack item3 = new ItemStack(Material.INK_SACK, 1, (short)15);
		ItemStack item5 = RecipesList.healingfruit();

		healingfruitgui.setItem(3, item3);
		healingfruitgui.setItem(4, item2);
		healingfruitgui.setItem(5, item3);
		healingfruitgui.setItem(12, item2);
		healingfruitgui.setItem(13, item1);
		healingfruitgui.setItem(14, item2);
		healingfruitgui.setItem(21, item3);
		healingfruitgui.setItem(22, item2);
		healingfruitgui.setItem(23, item3);
		healingfruitgui.setItem(40, item5);
		healingfruitgui.setItem(49, RecipesList.back2());

		p.openInventory(healingfruitgui);

	}
	
	public static Inventory enlighteningpackgui = Bukkit.createInventory(null, 54, RecipesList.enlighteningpack().getItemMeta().getDisplayName());

	public static void openenlighteningpackgui(Player p) {

		ItemStack item1 = new ItemStack(Material.REDSTONE_BLOCK);
		ItemStack item2 = new ItemStack(Material.GLASS_BOTTLE);
		ItemStack item5 = RecipesList.enlighteningpack();

		
		enlighteningpackgui.setItem(4, item1);
		
		enlighteningpackgui.setItem(12, item1);
		enlighteningpackgui.setItem(13, item2);
		enlighteningpackgui.setItem(14, item1);
		
		enlighteningpackgui.setItem(22, item1);
		
		enlighteningpackgui.setItem(40, item5);
		enlighteningpackgui.setItem(49, RecipesList.back2());

		p.openInventory(enlighteningpackgui);

	}
	
	public static Inventory lightenchanttablegui = Bukkit.createInventory(null, 54, RecipesList.lightenchanttable().getItemMeta().getDisplayName());

	public static void openlightenchanttablegui(Player p) {

		ItemStack item1 = new ItemStack(Material.BOOKSHELF);
		ItemStack item2 = new ItemStack(Material.OBSIDIAN);
		ItemStack item3 = new ItemStack(Material.DIAMOND);
		ItemStack item4 = new ItemStack(Material.EXP_BOTTLE);
		ItemStack item5 = RecipesList.lightenchanttable();

		
		lightenchanttablegui.setItem(4, item1);
		
		lightenchanttablegui.setItem(12, item2);
		lightenchanttablegui.setItem(13, item3);
		lightenchanttablegui.setItem(14, item2);
		lightenchanttablegui.setItem(21, item2);
		lightenchanttablegui.setItem(22, item4);
		lightenchanttablegui.setItem(23, item2);
		lightenchanttablegui.setItem(40, item5);
		lightenchanttablegui.setItem(49, RecipesList.back2());

		p.openInventory(lightenchanttablegui);

	}
	
	public static Inventory hermesbootsgui = Bukkit.createInventory(null, 54, RecipesList.hermesboots().getItemMeta().getDisplayName());

	public static void openhermesbootsgui(Player p) {

		ItemStack item1 = new ItemStack(Material.DIAMOND);
		ItemStack item2 = new ItemStack(Material.BLAZE_POWDER);
		ItemStack item3 = new ItemStack(Material.FEATHER);
		ItemStack item4 = new ItemStack(Material.DIAMOND_BOOTS);
		ItemStack item5 = RecipesList.heads();
		ItemStack item6 = RecipesList.hermesboots();

		hermesbootsgui.setItem(3, item1);
		hermesbootsgui.setItem(4, item5);
		hermesbootsgui.setItem(5, item1);
		hermesbootsgui.setItem(12, item2);
		hermesbootsgui.setItem(13, item4);
		hermesbootsgui.setItem(14, item2);
		hermesbootsgui.setItem(21, item3);
		
		hermesbootsgui.setItem(23, item3);
		hermesbootsgui.setItem(40, item5);
		hermesbootsgui.setItem(49, RecipesList.back2());

		p.openInventory(hermesbootsgui);

	}
	
	
	public static Inventory barbariangui = Bukkit.createInventory(null, 54, RecipesList.barbarian().getItemMeta().getDisplayName());

	public static void openbarbariangui(Player p) {

		ItemStack item1 = new ItemStack(Material.BLAZE_ROD);
		ItemStack item2 = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemStack item3 = new ItemStack(Material.IRON_BLOCK);
		ItemStack stpot = new ItemStack(Material.POTION, 1, (short) 8201);
		ItemStack item5 = RecipesList.barbarian();

		
		barbariangui.setItem(12, item1);
		barbariangui.setItem(13, item2);
		barbariangui.setItem(14, item1);
		barbariangui.setItem(21, item3);
		barbariangui.setItem(22, stpot);
		barbariangui.setItem(23, item3);
		barbariangui.setItem(40, item5);
		barbariangui.setItem(49, RecipesList.back2());

		p.openInventory(barbariangui);

	}
	
	public static Inventory corngui = Bukkit.createInventory(null, 54, RecipesList.corn().getItemMeta().getDisplayName());

	public static void opencorngui(Player p) {

		ItemStack item1 = new ItemStack(Material.CARROT_ITEM);
		ItemStack item2 = new ItemStack(Material.GOLDEN_APPLE);
		ItemStack item5 = RecipesList.corn();

		corngui.setItem(3, item1);
		corngui.setItem(4, item1);
		corngui.setItem(5, item1);
		corngui.setItem(12, item1);
		corngui.setItem(13, item2);
		corngui.setItem(14, item1);
		corngui.setItem(21, item1);
		corngui.setItem(22, item1);
		corngui.setItem(23, item1);
		corngui.setItem(40, item5);
		corngui.setItem(49, RecipesList.back2());

		p.openInventory(corngui);

	}
	
	public static Inventory kingsrodgui = Bukkit.createInventory(null, 54, RecipesList.kingsrod().getItemMeta().getDisplayName());

	public static void openkingsrodgui(Player p) {

		ItemStack item1 = new ItemStack(Material.FISHING_ROD);
		ItemStack item2 = new ItemStack(Material.WATER_LILY);
		ItemStack item3 = new ItemStack(Material.COMPASS);
		ItemStack item4 = new ItemStack(Material.WATER_BUCKET);
		ItemStack item5 = RecipesList.kingsrod();

		
		kingsrodgui.setItem(4, item1);
		
		kingsrodgui.setItem(12, item2);
		kingsrodgui.setItem(13, item3);
		kingsrodgui.setItem(14, item2);
		
		kingsrodgui.setItem(22, item4);

		kingsrodgui.setItem(40, item5);
		kingsrodgui.setItem(49, RecipesList.back2());

		p.openInventory(kingsrodgui);

	}
	
	
	
	
	
	
	

}
