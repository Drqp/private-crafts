package main.java.Guis.Items;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import main.java.UHC;
import main.java.Guis.newGUISystem.testGUI;
import main.java.Recipes.RecipesList;

public class RecipesGUI implements CommandExecutor, Listener {

	FileConfiguration config = UHC.plugin.getConfig();

	public static void mainrecipegui(Player p) {
		Inventory maingui = Bukkit.createInventory(null, 54, ChatColor.WHITE + "" + ChatColor.BOLD + "Recipes");

		ItemStack ironpack = RecipesList.IronPack();
		ItemMeta ironmeta = ironpack.getItemMeta();
		ironmeta.setDisplayName(ChatColor.GREEN + "Iron Pack");
		ironpack.setItemMeta(ironmeta);

		ItemStack goldpack = RecipesList.goldpack();
		ItemMeta goldmeta = goldpack.getItemMeta();
		goldmeta.setDisplayName(ChatColor.GREEN + "Gold Pack");
		goldpack.setItemMeta(goldmeta);

		maingui.setItem(10, RecipesList.VorpalSword());
		maingui.setItem(11, RecipesList.bos());
		maingui.setItem(12, RecipesList.bop());
		maingui.setItem(13, RecipesList.ghead());
		maingui.setItem(14, RecipesList.DragonSword());
		maingui.setItem(15, RecipesList.Anduril());
		maingui.setItem(16, RecipesList.QuickPick());
		maingui.setItem(19, RecipesList.SevenLeagueBoots());
		maingui.setItem(20, RecipesList.CupidsBow());
		maingui.setItem(21, ironpack);
		maingui.setItem(22, RecipesList.dragonchestplate());
		maingui.setItem(23, RecipesList.fusioncraftitem());
		maingui.setItem(24, goldpack);
		maingui.setItem(25, RecipesList.philos());
		maingui.setItem(28, RecipesList.exodus());
		maingui.setItem(29, RecipesList.saddle());
		maingui.setItem(30, RecipesList.tarnhelm());
		maingui.setItem(31, RecipesList.tabletofdestiny());
		maingui.setItem(32, RecipesList.spikedarmor());
		maingui.setItem(33, RecipesList.vidar());
		maingui.setItem(34, RecipesList.axeofperun());
		maingui.setItem(37, RecipesList.artbook());
		maingui.setItem(38, RecipesList.bookofthoth());
		maingui.setItem(39, RecipesList.fenrir());
		maingui.setItem(40, RecipesList.hideofl());
		maingui.setItem(41, RecipesList.nectar());
		maingui.setItem(42, RecipesList.potionoftoughness());
		maingui.setItem(43, RecipesList.holywater());
		maingui.setItem(53, RecipesList.nextpage());

		p.openInventory(maingui);

	}

	public static void mainrecipepg2gui(Player p) {
		Inventory mainguipg2 = Bukkit.createInventory(null, 54, ChatColor.WHITE + "" + ChatColor.BOLD + "Recipes");

		ItemStack ironpack = RecipesList.IronPack();
		ItemMeta ironmeta = ironpack.getItemMeta();
		ironmeta.setDisplayName(ChatColor.GREEN + "Iron Pack");
		ironpack.setItemMeta(ironmeta);

		ItemStack goldpack = RecipesList.goldpack();
		ItemMeta goldmeta = goldpack.getItemMeta();
		goldmeta.setDisplayName(ChatColor.GREEN + "Gold Pack");
		goldpack.setItemMeta(goldmeta);

		mainguipg2.setItem(10, RecipesList.lightapple());
		mainguipg2.setItem(11, RecipesList.artmeis());
		mainguipg2.setItem(12, RecipesList.pandorasbox());
		mainguipg2.setItem(13, RecipesList.deathsythe());
		mainguipg2.setItem(14, RecipesList.sugarrush());
		mainguipg2.setItem(15, RecipesList.arroweco());
		mainguipg2.setItem(16, RecipesList.lightanvil());
		mainguipg2.setItem(19, RecipesList.dustoflight());
		mainguipg2.setItem(20, RecipesList.brewingartifact());
		mainguipg2.setItem(21, RecipesList.netherartifact());
		mainguipg2.setItem(22, RecipesList.deliciousmeal());
		mainguipg2.setItem(23, RecipesList.evestemptation());
		mainguipg2.setItem(24, RecipesList.healingfruit());
		mainguipg2.setItem(25, RecipesList.enlighteningpack());
		mainguipg2.setItem(28, RecipesList.lightenchanttable());
		mainguipg2.setItem(29, RecipesList.hermesboots());
		mainguipg2.setItem(30, RecipesList.barbarian());
		mainguipg2.setItem(31, RecipesList.corn());
		mainguipg2.setItem(32, RecipesList.kingsrod());
		mainguipg2.setItem(45, RecipesList.prevpage());
		p.openInventory(mainguipg2);

	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (command.getName().equalsIgnoreCase("recipes")) {
			Player p = (Player) sender;
			if (p.hasPermission("uhc.recipes")) {
				testGUI.openTest(p, null, 0);
			}
		}
		return false;
	}

	@EventHandler
	public void OnClickEvent(InventoryClickEvent e) {

		Player p = (Player) e.getWhoClicked();

		if (ChatColor.stripColor(e.getInventory().getTitle()).equals("Recipes")) {
			if (e.getCurrentItem() == null)
				return;
			if (e.getCurrentItem().getItemMeta() == null)
				return;

			if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Book of Sharpening")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.bosgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Dragon Sword")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opendraggui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Cupid's Bow")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opencupidsbowgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Golden Head")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opengheadgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Vorpal Sword")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.VorpalGUI(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Book Of Protection")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.bopgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Anduril")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openandurilgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Quick Pick")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openquickpickgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Seven League Boots")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opensevenleaguegui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Iron Pack")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openironpackgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Dragon Chestplate")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opendragchestgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Fusion Armor")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openfusiongui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Gold Pack")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opengoldpackgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Philosophers Pickaxe")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openphilosgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Exodus")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openexodusgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Saddle")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opensaddlegui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Tarnhelm")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opentarnhelmgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Tablet Of Destiny")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opentabletofdestinygui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Spiked Armor")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openspikedarmorgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Axe Of Perun")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openaxeofperungui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Artemis Book")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.artemisbookgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Book Of Thoth")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openbookofthothgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Fenrir")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openfenrirgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Hide Of Leviathan")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openhideoflgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Shoes Of Vidar")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openVidargui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Nectar")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opennectargui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Potion Of Toughness")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openpotionoftoughnessgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Holy Water")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openholywatergui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Light Apple")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openlightapplegui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Artemis' Bow")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openartemisgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Pandoras Box")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openpboxgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Death's Sythe")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opendeathsythegui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Sugar Rush")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opensugarrushgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Arrow Economy")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openarrowecogui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Light Anvil")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openlightanvilgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Dust Of Light")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opendustoflightgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Brewing Artifact")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openbrewingartifactgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Nether Artifact")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opennetherartifactgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Delicious Meal")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opendeliciousmealgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Eve's Temptation")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openevestemptationgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Healing Fruit")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openhealingfruitgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Enlightening Pack")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openenlighteningpackgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Light Enchant Table")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openlightenchanttablegui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Hermes' Boots")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openhermesbootsgui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Barbarian Chestplate")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openbarbariangui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("Cornucopia")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.opencorngui(p);
			} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
					.equalsIgnoreCase("King's Rod")) {
				e.setCancelled(true);
				p.closeInventory();
				opengui.openkingsrodgui(p);
			}
		}
	}

	@EventHandler
	public void ClickEventBack(InventoryClickEvent e) {

		Player p = (Player) e.getWhoClicked();

		if (e.getCurrentItem().getType() == Material.ARROW) {
			if (e.getCurrentItem().getItemMeta().equals(RecipesList.back().getItemMeta())) {
				e.setCancelled(true);
				mainrecipegui(p);
			}

			if (e.getCurrentItem().getItemMeta().equals(RecipesList.nextpage().getItemMeta())) {
				e.setCancelled(true);
				mainrecipepg2gui(p);
			}
			if (e.getCurrentItem().getItemMeta().equals(RecipesList.prevpage().getItemMeta())) {
				e.setCancelled(true);
				mainrecipegui(p);
			}

			if (e.getCurrentItem().getItemMeta().equals(RecipesList.back2().getItemMeta())) {
				e.setCancelled(true);
				mainrecipepg2gui(p);
			}

		}
	}

}
