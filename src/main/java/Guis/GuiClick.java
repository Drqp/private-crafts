package main.java.Guis;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class GuiClick implements Listener {

	@EventHandler
	public void OnClickEvent(InventoryClickEvent e) {
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Book of Sharpening")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Golden Head")) {
			e.setCancelled(true);
		}

		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Cupid's Bow")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Dragon Sword")) {
			e.setCancelled(true);
		}

		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Vorpal Sword")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Book of Protection")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Anduril")) {
			e.setCancelled(true);
		} 
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Quick Pick")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Seven League Boots")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Iron Pack")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Dragon Chestplate")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Fusion Piece (Shapeless Craft)")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Gold Pack")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Philosophers Pickaxe")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Exodus")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Saddle")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Tarnhelm")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Tablet Of Destiny")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Spiked Armor")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Shoes Of Vidar")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Axe of Perun")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Artemis Book")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Fenrir")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Book Of Thoth")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Hide of Leviathan")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Nectar")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Potion Of Toughness")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Holy Water")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Light Apple")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Artemis Bow")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Pandoras Box")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Death Sythe")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Sugar Rush")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Arrow Economy")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Dust Of Light")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Brewing Artifact")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Nether Artifact")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Delicious Meal")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Eve's Temptation")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Healing Fruit")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Enlightening Pack")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Light Enchant Table")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Hermes' Boots")) {
			e.setCancelled(true);
		}
		
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Barbarian Chestplate")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Cornucopia")) {
			e.setCancelled(true);
		}
		if (ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("King's Rod")) {
			e.setCancelled(true);
		}
		

	}

}
