package main.java.Guis.newGUISystem;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

import main.java.utils.AllItems;

public class testGUI implements Listener {

	public static void openTest(Player p, ShapedRecipe r, int b) {
		Inventory inv = null;
		if (r == null) {

			inv = Bukkit.createInventory(null, 54, ChatColor.GRAY + "" + ChatColor.BOLD + "Recipes2");

			int d = -1;

			for (ItemStack is : AllItems.Items) {

				if (d < inv.getSize()) {

					ItemStack m = new ItemStack(is);

					d++;

					inv.setItem(d, m);

				}

			}

		} else {

			if (b == 1) {

				inv = Bukkit.createInventory(null, 45, "2");
				int g = 10;
				int y = 0;
				for (String is : r.getShape()) {

					for (int i = 0; i < is.length(); i++) {
						if (y == 3) {
							y = 0;
							g += 6;

						}
						inv.setItem(g, r.getIngredientMap().get(is.charAt(i)));
						y++;
						g++;
					}
					// i.setItem(g, r.getIngredientMap().get());
				}

			}
		}

		p.openInventory(inv);
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if (e.getInventory().getTitle().equals(ChatColor.GRAY + "" + ChatColor.BOLD + "Recipes2")) {

			if (e.getCurrentItem() != null) {

				for (ItemStack is : AllItems.Items) {

					if (e.getCurrentItem().getItemMeta().getDisplayName() == is.getItemMeta().getDisplayName()) {

						e.getWhoClicked().sendMessage("works" + AllItems.getRecipe(e.getCurrentItem()));

						openTest((Player) e.getWhoClicked(), AllItems.getRecipe(e.getCurrentItem()), 1);

					} else {
					}

				}
				e.setCancelled(true);
			} else if (e.getInventory().getTitle().equals("2")) {
				openTest((Player) e.getWhoClicked(), null, 0);
				e.setCancelled(true);
			}
		}
	}

}
