package main.java.Guis;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import main.java.UHC;
import main.java.Recipes.RecipesList;

public class ScenariosGui implements CommandExecutor, Listener {

	FileConfiguration config = UHC.plugin.getConfig();

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (command.getName().equalsIgnoreCase("scenarios")) {
			Player p = (Player) sender;
			if (p.hasPermission("uhc.scenarios")) {
				openGUI(p);
			} else {
				sender.sendMessage(ChatColor.DARK_RED + "No Permission!");
			}
		}
		return false;
	}

	public static void openGUI(Player p) {

		FileConfiguration config = UHC.plugin.getConfig();
		Inventory inv = Bukkit.createInventory(null, 54, ChatColor.AQUA + "Scenarios");
		ItemStack item = new ItemStack(Material.IRON_ORE);
		ItemMeta meta = item.getItemMeta();

		// Cut Clean
		if (config.getBoolean("CutClean") == true) {
			meta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "CUTCLEAN = ENABLED");
		} else {
			meta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "CUTCLEAN = DISABLED");
		}
		item.setItemMeta(meta);
		inv.setItem(11, item);

		// Stone Pick
		ItemStack stonepick = new ItemStack(Material.STONE_PICKAXE);
		ItemMeta stonepickmeta = stonepick.getItemMeta();
		stonepickmeta.addEnchant(Enchantment.DIG_SPEED, 3, true);

		if (config.getBoolean("EnchantedStoneTools") == true) {
			stonepickmeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "ENCHANTED STONE TOOLS = ENABLED");
		} else {
			stonepickmeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "ENCHANTED STONE TOOLS = DISABLED");
		}
		stonepick.setItemMeta(stonepickmeta);
		inv.setItem(13, stonepick);
		p.openInventory(inv);

		// Iron Pick
		ItemStack ironpick = new ItemStack(Material.IRON_PICKAXE);
		ItemMeta ironpickmeta = ironpick.getItemMeta();
		ironpickmeta.addEnchant(Enchantment.DIG_SPEED, 2, true);

		if (config.getBoolean("EnchantedIronTools") == true) {
			ironpickmeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "ENCHANTED IRON TOOLS = ENABLED");
		} else {
			ironpickmeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "ENCHANTED IRON TOOLS = DISABLED");
		}
		ironpick.setItemMeta(ironpickmeta);
		inv.setItem(15, ironpick);

		// Head
		ItemStack head = new ItemStack(Material.SKULL_ITEM);
		ItemMeta headmeta = head.getItemMeta();

		if (config.getBoolean("Heads") == true) {
			headmeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "PLAYER HEADS = ENABLED");
		} else {
			headmeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "PLAYER HEADS = DISABLED");
		}
		head.setItemMeta(headmeta);
		inv.setItem(28, head);

		// God Apples
		ItemStack godapple = new ItemStack(Material.GOLDEN_APPLE, 1, (short) 1);
		ItemMeta godapplemeta = godapple.getItemMeta();

		if (config.getBoolean("EnchantedGoldenApples") == true) {
			godapplemeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "ENCHANTED GOLDEN APPLES = ENABLED");
		} else {
			godapplemeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "ENCHANTED GOLDEN APPLES = DISABLED");
		}
		godapple.setItemMeta(godapplemeta);
		inv.setItem(30, godapple);

		// Custom Items
		ItemStack customitems = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemMeta customitemsmeta = customitems.getItemMeta();

		if (config.getBoolean("CustomItems") == true) {
			customitemsmeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "CUSTOM ITEMS = ENABLED");
		} else {
			customitemsmeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "CUSTOM ITEMS = DISABLED");
		}
		customitems.setItemMeta(customitemsmeta);
		inv.setItem(32, customitems);

		// Custom Items
		ItemStack ghead = RecipesList.ghead();
		ItemMeta gheadmeta = RecipesList.ghead().getItemMeta();

		if (config.getBoolean("GoldenHeads") == true) {
			gheadmeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "GOLDEN HEADS = ENABLED");
		} else {
			gheadmeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "GOLDEN HEADS = DISABLED");
		}
		ghead.setItemMeta(gheadmeta);
		inv.setItem(34, ghead);

		// Double Health
		ItemStack health = RecipesList.pancea();
		ItemMeta healthmeta = RecipesList.pancea().getItemMeta();

		if (config.getBoolean("DoubleHealth") == true) {
			healthmeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "DOUBLE HEALTH = ENABLED");
		} else {
			healthmeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "DOUBLE HEALTH = DISABLED");
		}
		health.setItemMeta(healthmeta);
		inv.setItem(49, health);

		p.openInventory(inv);
	}

	@EventHandler
	public void OnClick(InventoryClickEvent e) {

		FileConfiguration config = UHC.plugin.getConfig();

		if (!ChatColor.stripColor(e.getView().getTitle()).equalsIgnoreCase("Scenarios"))
			return;
		if (e.getCurrentItem() == null)
			return;
		if (e.getCurrentItem().getItemMeta() == null)
			return;

		Player p = (Player) e.getWhoClicked();

		if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("CUTCLEAN = ENABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("CutClean", false);
				UHC.plugin.saveConfig();
				p.closeInventory();
				openGUI(p);
			}
		} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("CUTCLEAN = DISABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("CutClean", true);
				UHC.plugin.saveConfig();
				p.closeInventory();
				openGUI(p);
			}
		} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("ENCHANTED STONE TOOLS = ENABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("EnchantedStoneTools", false);
				UHC.plugin.saveConfig();
				p.closeInventory();
				openGUI(p);
			}
		} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("ENCHANTED STONE TOOLS = DISABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("EnchantedStoneTools", true);
				UHC.plugin.saveConfig();
				p.closeInventory();
				openGUI(p);
			}
		} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("ENCHANTED IRON TOOLS = DISABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("EnchantedIronTools", true);
				UHC.plugin.saveConfig();
				p.closeInventory();
				openGUI(p);
			}
		} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("ENCHANTED IRON TOOLS = ENABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("EnchantedIronTools", false);
				UHC.plugin.saveConfig();
				p.closeInventory();
				openGUI(p);
			}
		} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("PLAYER HEADS = ENABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("Heads", false);
				UHC.plugin.saveConfig();
				p.closeInventory();
				openGUI(p);
			}
		} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("PLAYER HEADS = DISABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("Heads", true);
				UHC.plugin.saveConfig();
				p.closeInventory();
				openGUI(p);
			}
		} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("ENCHANTED GOLDEN APPLES = ENABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("EnchantedGoldenApples", false);
				UHC.plugin.saveConfig();
				p.closeInventory();
				openGUI(p);
			}
		} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("ENCHANTED GOLDEN APPLES = DISABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("EnchantedGoldenApples", true);
				UHC.plugin.saveConfig();
				p.closeInventory();
				openGUI(p);
			}
		} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("CUSTOM ITEMS = ENABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("CustomItems", false);
				UHC.plugin.saveConfig();
				p.closeInventory();
				openGUI(p);
			}
		} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("CUSTOM ITEMS = DISABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("CustomItems", true);
				UHC.plugin.saveConfig();
				p.closeInventory();
				openGUI(p);
			}
		} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("GOLDEN HEADS = ENABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("GoldenHeads", false);
				UHC.plugin.saveConfig();
				p.closeInventory();
				openGUI(p);
			}
		} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("GOLDEN HEADS = DISABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("GoldenHeads", true);
				UHC.plugin.saveConfig();
				p.closeInventory();
				openGUI(p);
			}
		} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("DOUBLE HEALTH = ENABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("DoubleHealth", false);
				p.sendMessage(ChatColor.RED + "WARNING! Relog for these changes to take place!");
				UHC.plugin.saveConfig();
				p.closeInventory();
				openGUI(p);
			}
		} else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())
				.equalsIgnoreCase("DOUBLE HEALTH = DISABLED")) {
			e.setCancelled(true);
			if (p.hasPermission("uhc.admin")) {
				config.set("DoubleHealth", true);
				UHC.plugin.saveConfig();
				p.sendMessage(ChatColor.RED + "WARNING! Relog for these changes to take place!");
				p.closeInventory();
				openGUI(p);
			}
		}
	}
}
