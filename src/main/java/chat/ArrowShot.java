package main.java.chat;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import main.java.UHC;
import net.md_5.bungee.api.ChatColor;

public class ArrowShot implements Listener {

	@EventHandler
	public void bowshot(EntityDamageByEntityEvent e) {
		
		FileConfiguration config = UHC.plugin.getConfig();


		if(config.getBoolean("Arrow-Messages") == true) {
		if (e.getEntity() instanceof Player) {

			Player damaged = (Player) e.getEntity();

			if (!(e.getDamager() instanceof Player)) {
				if (e.getDamager() instanceof Projectile) {
					if (((Projectile) e.getDamager()).getShooter() instanceof Player) {

						Player damager = (Player) (((Projectile) e.getDamager()).getShooter());

						double health = damaged.getHealth() - e.getDamage();
						damager.sendMessage(ChatColor.AQUA + "Hit " + ChatColor.RED + damaged.getDisplayName()
								+ ChatColor.AQUA + " for " + ChatColor.RED + e.getDamage() + ChatColor.AQUA
								+ " damage (" + ChatColor.GREEN + health + ChatColor.AQUA + " remaining).");
						damager.sendMessage(damaged.getDisplayName() + ChatColor.YELLOW + " is on " + ChatColor.RED
								+ health + ChatColor.YELLOW + " HP!");
					}
				}
			}
		}
	}
	}
}
