package main.java.Recipes;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import main.java.UHC;
import main.java.utils.CustomMeta;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagInt;
import net.minecraft.server.v1_8_R3.NBTTagList;
import net.minecraft.server.v1_8_R3.NBTTagString;

public class RecipesList {

	public static ItemStack heads() {
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		CustomMeta.setMeta(head, "normal");
		ItemMeta headmeta = head.getItemMeta();
		headmeta.setDisplayName(ChatColor.RED + "Player Head (Right Click)");
		head.setItemMeta(headmeta);
		return head;
	}

	public static ItemStack ghead() {
		ItemStack ghead = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		ghead = CustomMeta.setMeta(ghead, "normal");
		ItemMeta gheadmeta = ghead.getItemMeta();
		gheadmeta.setDisplayName(ChatColor.GOLD + "Golden Head");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Absorption (2:00)");
		lore.add(ChatColor.GRAY + "Regeneration II (0:05)");
		gheadmeta.setLore(lore);
		ghead.setItemMeta(gheadmeta);
		return ghead;
	}

	public static ItemStack QuickPick() {
		ItemStack quickpick = new ItemStack(Material.IRON_PICKAXE);
		quickpick = CustomMeta.setMeta(quickpick, "normal");
		ItemMeta quickpickmeta = quickpick.getItemMeta();
		quickpickmeta.setDisplayName(ChatColor.GREEN + "Quick Pick");
		quickpickmeta.addEnchant(Enchantment.DIG_SPEED, 1, true);
		quickpick.setItemMeta(quickpickmeta);
		return quickpick;
	}

	public static ItemStack SevenLeagueBoots() {
		ItemStack sevenleague = new ItemStack(Material.DIAMOND_BOOTS);
		sevenleague = CustomMeta.setMeta(sevenleague, "ultimate");
		ItemMeta sevenleaguemeta = sevenleague.getItemMeta();
		sevenleaguemeta.setDisplayName(ChatColor.GREEN + "Seven League Boots");
		sevenleaguemeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
		sevenleaguemeta.addEnchant(Enchantment.PROTECTION_FALL, 3, true);
		sevenleague.setItemMeta(sevenleaguemeta);
		return sevenleague;
	}

	public static ItemStack bos() {
		ItemStack bookofsharpening = new ItemStack(Material.ENCHANTED_BOOK);
		bookofsharpening = CustomMeta.setMeta(bookofsharpening, "normal");
		EnchantmentStorageMeta bookofsharpeningmeta = (EnchantmentStorageMeta) bookofsharpening.getItemMeta();
		bookofsharpeningmeta.setDisplayName(ChatColor.YELLOW + "Book of Sharpening");
		bookofsharpeningmeta.addStoredEnchant(Enchantment.DAMAGE_ALL, 1, true);
		bookofsharpening.setItemMeta(bookofsharpeningmeta);
		return bookofsharpening;
	}

	public static ItemStack bop() {
		ItemStack bookofprotection = new ItemStack(Material.ENCHANTED_BOOK);
		bookofprotection = CustomMeta.setMeta(bookofprotection, "normal");

		EnchantmentStorageMeta bookofprotectionmeta = (EnchantmentStorageMeta) bookofprotection.getItemMeta();
		bookofprotectionmeta.setDisplayName(ChatColor.YELLOW + "Book of Protection");
		bookofprotectionmeta.addStoredEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
		bookofprotection.setItemMeta(bookofprotectionmeta);
		return bookofprotection;
	}

	public static ItemStack bopower() {
		ItemStack bookofpower = new ItemStack(Material.ENCHANTED_BOOK);
		bookofpower = CustomMeta.setMeta(bookofpower, "normal");
		EnchantmentStorageMeta bookofpowermeta = (EnchantmentStorageMeta) bookofpower.getItemMeta();
		bookofpowermeta.setDisplayName(ChatColor.YELLOW + "Book of Power");
		bookofpowermeta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 1, true);
		bookofpower.setItemMeta(bookofpowermeta);
		return bookofpower;
	}

	public static ItemStack CupidsBow() {
		ItemStack cupidsbow = new ItemStack(Material.BOW);
		cupidsbow = CustomMeta.setMeta(cupidsbow, "normal");
		ItemMeta cupidsbowmeta = cupidsbow.getItemMeta();
		cupidsbowmeta.setDisplayName(ChatColor.RED + "Cupid's Bow");
		cupidsbowmeta.addEnchant(Enchantment.ARROW_DAMAGE, 1, true);
		cupidsbowmeta.addEnchant(Enchantment.ARROW_FIRE, 1, true);
		cupidsbow.setItemMeta(cupidsbowmeta);
		return cupidsbow;
	}

	public static ItemStack IronPack() {
		ItemStack ironpack = new ItemStack(Material.IRON_INGOT, 9);
		ironpack = CustomMeta.setMeta(ironpack, "normal");
		return ironpack;
	}

	public static ItemStack DragonSword() {
		ItemStack dragonsword = new ItemStack(Material.DIAMOND_SWORD);
		ItemMeta dragonswordmeta = dragonsword.getItemMeta();
		dragonswordmeta.setDisplayName(ChatColor.GREEN + "Dragon Sword");
		ArrayList<String> dragonlore = new ArrayList<String>();
		dragonlore.add(ChatColor.GRAY + "Weaponsmith Ultimate");
		dragonswordmeta.setLore(dragonlore);
		dragonsword.setItemMeta(dragonswordmeta);
		net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(dragonsword);
		NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
		NBTTagList modifiers = new NBTTagList();
		NBTTagCompound damage = new NBTTagCompound();
		damage.set("AttributeName", new NBTTagString("generic.attackDamage"));
		damage.set("Name", new NBTTagString("generic.attackDamage"));
		damage.set("Amount", new NBTTagInt((int) 8.25));
		damage.set("Operation", new NBTTagInt(0));
		damage.set("UUIDLeast", new NBTTagInt(894654));
		damage.set("UUIDMost", new NBTTagInt(2872));
		modifiers.add(damage);
		compound.set("AttributeModifiers", modifiers);
		compound.setString("Rarity", "ultimate");
		nmsStack.setTag(compound);
		dragonsword = CraftItemStack.asBukkitCopy(nmsStack);
		return dragonsword;
	}

	public static ItemStack VorpalSword() {
		ItemStack vorpalsword = new ItemStack(Material.IRON_SWORD);
		vorpalsword = CustomMeta.setMeta(vorpalsword, "normal");
		ItemMeta vorpalswordmeta = vorpalsword.getItemMeta();
		vorpalswordmeta.setDisplayName(ChatColor.GREEN + "Vorpal Sword");
		vorpalswordmeta.addEnchant(Enchantment.DAMAGE_UNDEAD, 2, true);
		vorpalswordmeta.addEnchant(Enchantment.LOOT_BONUS_MOBS, 1, true);
		vorpalswordmeta.addEnchant(Enchantment.DAMAGE_ARTHROPODS, 2, true);
		vorpalsword.setItemMeta(vorpalswordmeta);
		return vorpalsword;

	}

	public static ItemStack Anduril() {
		ItemStack anduril = new ItemStack(Material.IRON_SWORD);
		anduril = CustomMeta.setMeta(anduril, "ultimate");
		ItemMeta andurilmeta = anduril.getItemMeta();
		andurilmeta.setDisplayName(ChatColor.GREEN + "Anduril");
		andurilmeta.addEnchant(Enchantment.DAMAGE_ALL, 2, true);
		anduril.setItemMeta(andurilmeta);
		return anduril;
	}

	public static ItemStack dragonchestplate() {
		ItemStack dragchest = new ItemStack(Material.DIAMOND_CHESTPLATE);
		dragchest = CustomMeta.setMeta(dragchest, "ultimate");
		ItemMeta dragchestmeta = dragchest.getItemMeta();
		dragchestmeta.setDisplayName(ChatColor.GREEN + "Dragon Chestplate");
		dragchestmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
		dragchest.setItemMeta(dragchestmeta);
		return dragchest;
	}

	public static ItemStack fusioncraftitem() {
		ItemStack fusionitem = new ItemStack(Material.CHEST);
		fusionitem = CustomMeta.setMeta(fusionitem, "ultimate");
		ItemMeta fusionitemmeta = fusionitem.getItemMeta();
		fusionitemmeta.setDisplayName(ChatColor.RED + "Fusion Armor");
		ArrayList<String> fusionitemlore = new ArrayList<String>();
		fusionitemlore.add("");
		fusionitemlore.add("Right Click This Item To Recieve A Random Fusion Piece");
		fusionitemlore.add("");
		fusionitemmeta.setLore(fusionitemlore);
		fusionitemmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 10, true);
		fusionitem.setItemMeta(fusionitemmeta);
		return fusionitem;
	}

	public static ItemStack fusionhelmet() {
		ItemStack fusionhelmet = new ItemStack(Material.DIAMOND_HELMET);
		ItemMeta fusionhelmetmeta = fusionhelmet.getItemMeta();
		fusionhelmetmeta.setDisplayName(ChatColor.RED + "Fusion Helmet");
		fusionhelmetmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 5, true);
		fusionhelmet.setItemMeta(fusionhelmetmeta);

		return fusionhelmet;
	}

	public static ItemStack fusionchestplate() {
		ItemStack fusionchestplate = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemMeta fusionchestplatemeta = fusionchestplate.getItemMeta();
		fusionchestplatemeta.setDisplayName(ChatColor.RED + "Fusion Chestplate");
		fusionchestplatemeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 5, true);
		fusionchestplate.setItemMeta(fusionchestplatemeta);
		return fusionchestplate;
	}

	public static ItemStack fusionlegs() {
		ItemStack fusionleggings = new ItemStack(Material.DIAMOND_LEGGINGS);
		ItemMeta fusionleggingsmeta = fusionleggings.getItemMeta();
		fusionleggingsmeta.setDisplayName(ChatColor.RED + "Fusion Leggings");
		fusionleggingsmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 5, true);
		fusionleggings.setItemMeta(fusionleggingsmeta);
		return fusionleggings;
	}

	public static ItemStack fusionboots() {
		ItemStack fusionboots = new ItemStack(Material.DIAMOND_BOOTS);
		ItemMeta fusionbootsmeta = fusionboots.getItemMeta();
		fusionbootsmeta.setDisplayName(ChatColor.RED + "Fusion Boots");
		fusionbootsmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 5, true);
		fusionboots.setItemMeta(fusionbootsmeta);
		return fusionboots;
	}

	public static ItemStack goldpack() {
		ItemStack goldpack = new ItemStack(Material.GOLD_INGOT, 9);
		goldpack = CustomMeta.setMeta(goldpack, "normal");
		return goldpack;
	}

	public static ItemStack philos() {
		ItemStack philosopherspickaxe = new ItemStack(Material.DIAMOND_PICKAXE);
		philosopherspickaxe = CustomMeta.setMeta(philosopherspickaxe, "ultimate");
		ItemMeta philosmeta = philosopherspickaxe.getItemMeta();
		philosmeta.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, 2, true);
		philosmeta.setDisplayName(ChatColor.RED + "Philosophers Pickaxe");
		philosopherspickaxe.setDurability((short) 1557);
		philosopherspickaxe.setItemMeta(philosmeta);
		return philosopherspickaxe;
	}

	public static ItemStack exodus() {
		ItemStack exodus = new ItemStack(Material.DIAMOND_HELMET);
		exodus = CustomMeta.setMeta(exodus, "ultimate");
		ItemMeta exodusmeta = exodus.getItemMeta();
		exodusmeta.setDisplayName(ChatColor.GOLD + "Exodus");
		exodus.setItemMeta(exodusmeta);
		return exodus;
	}

	public static ItemStack saddle() {
		ItemStack saddle = new ItemStack(Material.SADDLE);
		saddle = CustomMeta.setMeta(saddle, "normal");
		ItemMeta meta = saddle.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Saddle");
		saddle.setItemMeta(meta);
		return saddle;
	}

	public static ItemStack tarnhelm() {
		ItemStack tarnhelmet = new ItemStack(Material.DIAMOND_HELMET);
		CustomMeta.setMeta(tarnhelmet, "normal");

		ItemMeta tarnhelmmeta = tarnhelmet.getItemMeta();
		tarnhelmmeta.setDisplayName(ChatColor.GREEN + "Tarnhelm");
		tarnhelmmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
		tarnhelmmeta.addEnchant(Enchantment.PROTECTION_FIRE, 1, true);
		tarnhelmmeta.addEnchant(Enchantment.OXYGEN, 1, true);
		tarnhelmet.setItemMeta(tarnhelmmeta);
		return tarnhelmet;
	}

	public static ItemStack tabletofdestiny() {
		ItemStack tablet = new ItemStack(Material.ENCHANTED_BOOK);
		tablet = CustomMeta.setMeta(tablet, "ultimate");
		EnchantmentStorageMeta tabletmeta = (EnchantmentStorageMeta) tablet.getItemMeta();
		tabletmeta.setDisplayName(ChatColor.GOLD + "Tablet Of Destiny");
		tabletmeta.addStoredEnchant(Enchantment.DAMAGE_ALL, 3, true);
		tabletmeta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 3, true);
		tabletmeta.addStoredEnchant(Enchantment.ARROW_KNOCKBACK, 1, true);
		tabletmeta.addStoredEnchant(Enchantment.FIRE_ASPECT, 2, true);
		tablet.setItemMeta(tabletmeta);
		return tablet;
	}

	public static ItemStack spikedarmor() {
		ItemStack spiked = new ItemStack(Material.LEATHER_CHESTPLATE, 1, (short) 1);
		CustomMeta.setMeta(spiked, "ultimate");
		ItemMeta spikedmeta = spiked.getItemMeta();
		spikedmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 5, true);
		spikedmeta.addEnchant(Enchantment.DURABILITY, 10, true);
		spikedmeta.addEnchant(Enchantment.THORNS, 1, true);
		spikedmeta.setDisplayName(ChatColor.GREEN + "Spiked Armor");
		spiked.setItemMeta(spikedmeta);
		return spiked;
	}

	public static ItemStack vidar() {
		ItemStack vidarboots = new ItemStack(Material.DIAMOND_BOOTS);
		vidarboots = CustomMeta.setMeta(vidarboots, "ultimate");

		ItemMeta vidarbootsmeta = vidarboots.getItemMeta();
		vidarbootsmeta.setDisplayName(ChatColor.GREEN + "Shoes of Vidar");
		vidarbootsmeta.addEnchant(Enchantment.DEPTH_STRIDER, 2, true);
		vidarbootsmeta.addEnchant(Enchantment.DURABILITY, 3, true);
		vidarbootsmeta.addEnchant(Enchantment.PROTECTION_PROJECTILE, 2, true);
		vidarbootsmeta.addEnchant(Enchantment.THORNS, 1, true);
		vidarboots.setItemMeta(vidarbootsmeta);
		return vidarboots;
	}

	public static ItemStack axeofperun() {

		ItemStack perun = new ItemStack(Material.DIAMOND_AXE);
		perun = CustomMeta.setMeta(perun, "ultimate");
		ItemMeta perunmeta = perun.getItemMeta();
		perunmeta.addEnchant(Enchantment.DURABILITY, 1, true);
		perunmeta.setDisplayName(ChatColor.GOLD + "Axe of Perun");
		perun.setItemMeta(perunmeta);
		return perun;
	}

	public static ItemStack artbook() {
		ItemStack artbook = new ItemStack(Material.ENCHANTED_BOOK);
		CustomMeta.setMeta(artbook, "normal");
		EnchantmentStorageMeta artbookmeta = (EnchantmentStorageMeta) artbook.getItemMeta();
		artbookmeta.addStoredEnchant(Enchantment.PROTECTION_PROJECTILE, 1, true);
		artbookmeta.setDisplayName(ChatColor.YELLOW + "Artemis Book");
		artbook.setItemMeta(artbookmeta);

		return artbook;
	}

	public static ItemStack head() {
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		ItemMeta headmeta = head.getItemMeta();
		headmeta.setDisplayName(ChatColor.RED + "Player Head (Right Click)");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("");
		lore.add(ChatColor.RED + "Right Click to recieve temporary speed and regeneration");
		headmeta.setLore(lore);
		head.setItemMeta(headmeta);
		return head;
	}

	public static ItemStack bookofthoth() {
		ItemStack bookofthoth = new ItemStack(Material.ENCHANTED_BOOK);
		bookofthoth = CustomMeta.setMeta(bookofthoth, "ultimate");
		ItemMeta thothmeta = bookofthoth.getItemMeta();
		thothmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
		thothmeta.addEnchant(Enchantment.DAMAGE_ALL, 2, true);
		thothmeta.addEnchant(Enchantment.ARROW_DAMAGE, 2, true);
		thothmeta.addEnchant(Enchantment.ARROW_KNOCKBACK, 1, true);
		thothmeta.addEnchant(Enchantment.FIRE_ASPECT, 1, true);
		thothmeta.setDisplayName(ChatColor.GREEN + "Book Of Thoth");
		bookofthoth.setItemMeta(thothmeta);
		return bookofthoth;
	}

	public static ItemStack fenrir() {
		ItemStack fenrir = new ItemStack(Material.MONSTER_EGG, 1, (short) 95);
		fenrir = CustomMeta.setMeta(fenrir, "ultimate");
		ItemMeta fenrirmeta = fenrir.getItemMeta();
		fenrirmeta.setDisplayName(ChatColor.RED + "Fenrir");
		ArrayList<String> fenlore = new ArrayList<String>();
		fenlore.add(ChatColor.GREEN + "Type: " + ChatColor.GRAY + "Wolf");
		fenlore.add(ChatColor.GREEN + "Attributes: ");
		fenlore.add(ChatColor.GOLD + "  -" + ChatColor.GRAY + "Speed");
		fenlore.add(ChatColor.GOLD + "  -" + ChatColor.GRAY + "Damage Resistance");
		fenlore.add(ChatColor.YELLOW + "Already tamed and loyal to you.");
		fenrirmeta.setLore(fenlore);
		fenrir.setItemMeta(fenrirmeta);
		return fenrir;
	}

	public static ItemStack hideofl() {
		ItemStack hide = new ItemStack(Material.DIAMOND_LEGGINGS);
		hide = CustomMeta.setMeta(hide, "ultimate");
		ItemMeta hidemeta = hide.getItemMeta();
		hidemeta.setDisplayName(ChatColor.GREEN + "Hide of Leviathan");
		hidemeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
		hidemeta.addEnchant(Enchantment.OXYGEN, 5, true);
		hidemeta.addEnchant(Enchantment.WATER_WORKER, 1, true);
		hide.setItemMeta(hidemeta);
		return hide;
	}

	public static ItemStack chestoffate() {
		ItemStack cofitem = new ItemStack(Material.CHEST);
		cofitem = CustomMeta.setMeta(cofitem, "ultimate");
		ItemMeta cofitemmeta = cofitem.getItemMeta();
		cofitemmeta.setDisplayName(ChatColor.GREEN + "Chest Of Fate");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GOLD + "Open it and see what you can find.");
		cofitemmeta.setLore(lore);
		cofitem.setItemMeta(cofitemmeta);
		return cofitem;
	}

	public static ItemStack nectar() {
		ItemStack nectaritem = new ItemStack(Material.POTION, 1, (short) 8193);
		nectaritem = CustomMeta.setMeta(nectaritem, "ultimate");
		PotionMeta nectarmeta = (PotionMeta) nectaritem.getItemMeta();
		nectarmeta.addCustomEffect(new PotionEffect(PotionEffectType.REGENERATION, 200, 2), true);
		nectarmeta.setDisplayName(ChatColor.GREEN + "Nectar");
		nectaritem.setItemMeta(nectarmeta);
		return nectaritem;
	}

	public static ItemStack potionoftoughness() {
		ItemStack potionoftoughnessitem = new ItemStack(Material.POTION);
		potionoftoughnessitem = CustomMeta.setMeta(potionoftoughnessitem, "normal");
		PotionMeta toughnessmeta = (PotionMeta) potionoftoughnessitem.getItemMeta();
		toughnessmeta.addCustomEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 2400, 0), true);
		toughnessmeta.setDisplayName(ChatColor.RED + "Potion Of Toughness");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Resistance (2:00)");
		toughnessmeta.setLore(lore);
		potionoftoughnessitem.setItemMeta(toughnessmeta);
		return potionoftoughnessitem;
	}

	// MAKE SPLASHABLE
	public static ItemStack potionofvelocity() {
		ItemStack velocityitem = new ItemStack(Material.POTION, 1, (short) 16386);
		velocityitem = CustomMeta.setMeta(velocityitem, "normal");
		PotionMeta velocitymeta = (PotionMeta) velocityitem.getItemMeta();
		velocitymeta.addCustomEffect(new PotionEffect(PotionEffectType.SPEED, 240, 2), true);
		velocitymeta.setDisplayName(ChatColor.GREEN + "Potion Of Velocity");
		velocitymeta.addEnchant(Enchantment.DURABILITY, 1, true);
		velocityitem.setItemMeta(velocitymeta);
		return velocityitem;
	}

	public static ItemStack holywater() {
		ItemStack holywateritem = new ItemStack(Material.POTION, 1, (short) 8193);
		holywateritem = CustomMeta.setMeta(holywateritem, "normal");
		PotionMeta holywatermeta = (PotionMeta) holywateritem.getItemMeta();
		holywatermeta.addCustomEffect(new PotionEffect(PotionEffectType.ABSORPTION, 2400, 1), true);
		holywatermeta.setDisplayName(ChatColor.GREEN + "Holy Water");
		holywatermeta.addEnchant(Enchantment.DURABILITY, 1, true);
		holywateritem.setItemMeta(holywatermeta);
		return holywateritem;
	}

	public static ItemStack pancea() {
		ItemStack panceaitem = new ItemStack(Material.POTION, 1, (short) 8197);
		panceaitem = CustomMeta.setMeta(panceaitem, "ultimate");
		PotionMeta panceameta = (PotionMeta) panceaitem.getItemMeta();
		panceameta.addCustomEffect(new PotionEffect(PotionEffectType.HEAL, 20, 4), true);
		panceameta.addCustomEffect(new PotionEffect(PotionEffectType.ABSORPTION, 1200, 1), true);
		panceameta.setDisplayName(ChatColor.GREEN + "Pancea");
		panceameta.addEnchant(Enchantment.DURABILITY, 1, true);
		panceaitem.setItemMeta(panceameta);
		return panceaitem;
	}

	public static ItemStack artmeis() {
		ItemStack artemisbow = new ItemStack(Material.BOW);
		artemisbow = CustomMeta.setMeta(artemisbow, "ultimate");
		ItemMeta artmeta = artemisbow.getItemMeta();
		artmeta.setDisplayName(ChatColor.RED + "Artemis' Bow");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.BLUE + "Hunting");
		artmeta.addEnchant(Enchantment.ARROW_DAMAGE, 3, true);
		artmeta.setLore(lore);
		artemisbow.setItemMeta(artmeta);
		return artemisbow;
	}

	public static ItemStack pandorasbox() {
		ItemStack pandorasbox = new ItemStack(Material.CHEST);
		CustomMeta.setMeta(pandorasbox, "normal");
		ItemMeta pandorasmeta = pandorasbox.getItemMeta();
		pandorasmeta.setDisplayName(ChatColor.GREEN + "Pandoras Box");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.RED + "Open to get a random item!");
		pandorasmeta.setLore(lore);
		pandorasbox.setItemMeta(pandorasmeta);
		return pandorasbox;
	}

	public static ItemStack flaskofichor() {
		ItemStack flaskitem = new ItemStack(Material.POTION, 1, (short) 16386);
		flaskitem = CustomMeta.setMeta(flaskitem, "normal");
		PotionMeta flaskmeta = (PotionMeta) flaskitem.getItemMeta();
		flaskmeta.addCustomEffect(new PotionEffect(PotionEffectType.HARM, 20, 2), true);
		flaskmeta.addEnchant(Enchantment.DURABILITY, 1, true);
		flaskmeta.setDisplayName(ChatColor.RED + "Flask Of Ichor");
		flaskitem.setItemMeta(flaskmeta);
		return flaskitem;
	}

	public static ItemStack excalibur() {
		ItemStack excalibur = new ItemStack(Material.DIAMOND_SWORD);
		excalibur = CustomMeta.setMeta(excalibur, "ultimate");
		ItemMeta excaliburmeta = excalibur.getItemMeta();
		excaliburmeta.addEnchant(Enchantment.DURABILITY, 1, true);
		excaliburmeta.setDisplayName(ChatColor.GOLD + "Excalibur");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.BLUE + "Chaos");
		lore.add("");
		lore.add("");
		lore.add(ChatColor.GOLD + "The Holy Sword");
		lore.add("");
		excaliburmeta.setLore(lore);
		excalibur.setItemMeta(excaliburmeta);
		return excalibur;
	}

	public static ItemStack voidbox() {
		ItemStack voidboxitem = new ItemStack(Material.ENDER_CHEST, 2);
		voidboxitem = CustomMeta.setMeta(voidboxitem, "ultimate");
		return voidboxitem;
	}

	public static ItemStack daredevil() {
		ItemStack daredevilitem = new ItemStack(Material.MONSTER_EGG, 1, (short) 101);
		daredevilitem = CustomMeta.setMeta(daredevilitem, "ultimate");

		ItemMeta daremeta = daredevilitem.getItemMeta();
		daremeta.setDisplayName(ChatColor.RED + "Dare Devil");
		ArrayList<String> darelore = new ArrayList<String>();
		darelore.add(ChatColor.GREEN + "Type: " + ChatColor.GRAY + "Horse");
		darelore.add(ChatColor.GREEN + "Attributes: ");
		darelore.add(ChatColor.GOLD + "  -" + ChatColor.GRAY + "Speed");
		darelore.add(ChatColor.GOLD + "  -" + ChatColor.GRAY + "Damage Resistance");
		darelore.add(ChatColor.YELLOW + "Already tamed and loyal to you.");
		daremeta.setLore(darelore);
		daredevilitem.setItemMeta(daremeta);
		return daredevilitem;
	}

	public static ItemStack deathsythe() {
		ItemStack deathsytheitem = new ItemStack(Material.IRON_HOE);
		deathsytheitem = CustomMeta.setMeta(deathsytheitem, "ultimate");
		ItemMeta deathsythemeta = deathsytheitem.getItemMeta();
		deathsythemeta.setDisplayName(ChatColor.LIGHT_PURPLE + "Death's Sythe");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.BLUE + "Oblivion");
		deathsythemeta.setLore(lore);
		deathsytheitem.setItemMeta(deathsythemeta);
		return deathsytheitem;
	}

	public static ItemStack hermesboots() {
		ItemStack hermesbootsitem = new ItemStack(Material.DIAMOND_BOOTS);
		hermesbootsitem = CustomMeta.setMeta(hermesbootsitem, "ultimate");
		ItemMeta hermesbootsmeta = hermesbootsitem.getItemMeta();
		hermesbootsmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, false);
		hermesbootsmeta.addEnchant(Enchantment.PROTECTION_FALL, 1, false);
		hermesbootsmeta.addEnchant(Enchantment.DURABILITY, 2, false);
		hermesbootsmeta.setDisplayName(ChatColor.GREEN + "Hermes' Boots");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Provides the wearer with 10% increase walk speed!");
		hermesbootsmeta.setLore(lore);
		hermesbootsitem.setItemMeta(hermesbootsmeta);
		return hermesbootsitem;
	}

	public static ItemStack barbarian() {
		ItemStack barbarianchestplate = new ItemStack(Material.DIAMOND_CHESTPLATE);
		barbarianchestplate = CustomMeta.setMeta(barbarianchestplate, "ultimate");
		ItemMeta meta = barbarianchestplate.getItemMeta();
		meta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
		meta.setDisplayName(ChatColor.GREEN + "Barbarian Chestplate");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.BLUE + "Strength 1 (While Wearing)");
		lore.add(ChatColor.BLUE + "Resistance 1 (While Wearing)");
		lore.add(ChatColor.BLUE + "Rage");
		meta.setLore(lore);
		barbarianchestplate.setItemMeta(meta);
		return barbarianchestplate;
	}

	public static ItemStack corn() {
		ItemStack cornitem = new ItemStack(Material.GOLDEN_CARROT, 5);
		cornitem = CustomMeta.setMeta(cornitem, "normal");
		ItemMeta meta = cornitem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Cornucopia");
		cornitem.setItemMeta(meta);
		return cornitem;
	}

	public static ItemStack kingsrod() {
		ItemStack kingsroditem = new ItemStack(Material.FISHING_ROD);
		kingsroditem = CustomMeta.setMeta(kingsroditem, "ultimate");
		ItemMeta meta = kingsroditem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "King's Rod");
		meta.addEnchant(Enchantment.LUCK, 10, true);
		meta.addEnchant(Enchantment.LURE, 5, true);
		meta.addEnchant(Enchantment.DURABILITY, 10, true);
		kingsroditem.setItemMeta(meta);
		return kingsroditem;
	}

	public static ItemStack diceofgod() {
		ItemStack diceitem = new ItemStack(Material.ENDER_PORTAL_FRAME);
		diceitem = CustomMeta.setMeta(diceitem, "ultimate");
		ItemMeta meta = diceitem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Dice of God");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Tempt the fate of the gods!");
		meta.setLore(lore);
		diceitem.setItemMeta(meta);
		return diceitem;
	}

	public static ItemStack back() {
		ItemStack backitem = new ItemStack(Material.ARROW);
		ItemMeta backmeta = backitem.getItemMeta();
		backmeta.setDisplayName(ChatColor.GREEN + "Back");
		backitem.setItemMeta(backmeta);
		return backitem;
	}

	public static ItemStack back2() {
		ItemStack backitem = new ItemStack(Material.ARROW);
		ItemMeta backmeta = backitem.getItemMeta();
		backmeta.setDisplayName(ChatColor.GREEN + "Back (Page 2)");
		backitem.setItemMeta(backmeta);
		return backitem;
	}

	public static ItemStack nextpage() {
		ItemStack nextpageitem = new ItemStack(Material.ARROW);
		ItemMeta nextpagemeta = nextpageitem.getItemMeta();
		nextpagemeta.setDisplayName(ChatColor.GREEN + "Next Page (Page 2)");
		nextpageitem.setItemMeta(nextpagemeta);
		return nextpageitem;
	}

	public static ItemStack prevpage() {
		ItemStack prevpageitem = new ItemStack(Material.ARROW);
		ItemMeta prevpagemeta = prevpageitem.getItemMeta();
		prevpagemeta.setDisplayName(ChatColor.GREEN + "Previous Page (Page 1)");
		prevpageitem.setItemMeta(prevpagemeta);
		return prevpageitem;
	}

	public static ItemStack lightapple() {
		ItemStack lightappleitem = new ItemStack(Material.GOLDEN_APPLE);
		lightappleitem = CustomMeta.setMeta(lightappleitem, "ultimate");
		ItemMeta meta = lightappleitem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Light Apple");
		lightappleitem.setItemMeta(meta);
		return lightappleitem;
	}

	public static ItemStack arroweco() {
		ItemStack arrowecoitem = new ItemStack(Material.ARROW, 20);
		arrowecoitem = CustomMeta.setMeta(arrowecoitem, "normal");
		ItemMeta meta = arrowecoitem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Arrow Economy");
		arrowecoitem.setItemMeta(meta);
		return arrowecoitem;
	}

	public static ItemStack lightanvil() {
		ItemStack lightanvilitem = new ItemStack(Material.ANVIL);
		lightanvilitem = CustomMeta.setMeta(lightanvilitem, "normal");
		ItemMeta meta = lightanvilitem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Light Anvil");
		lightanvilitem.setItemMeta(meta);
		return lightanvilitem;
	}

	public static ItemStack dustoflight() {
		ItemStack dustoflightitem = new ItemStack(Material.GLOWSTONE_DUST, 8);
		dustoflightitem = CustomMeta.setMeta(dustoflightitem, "normal");
		ItemMeta meta = dustoflightitem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Dust Of Light");
		dustoflightitem.setItemMeta(meta);
		return dustoflightitem;
	}

	public static ItemStack brewingartifact() {
		ItemStack brewingartifactitem = new ItemStack(Material.NETHER_STALK);
		brewingartifactitem = CustomMeta.setMeta(brewingartifactitem, "normal");
		ItemMeta meta = brewingartifactitem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Brewing Artifact");
		brewingartifactitem.setItemMeta(meta);
		return brewingartifactitem;
	}

	public static ItemStack netherartifact() {
		ItemStack netherartifactitem = new ItemStack(Material.BLAZE_ROD);
		netherartifactitem = CustomMeta.setMeta(netherartifactitem, "normal");
		ItemMeta meta = netherartifactitem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Nether Artifact");
		netherartifactitem.setItemMeta(meta);
		return netherartifactitem;
	}

	public static ItemStack deliciousmeal() {
		ItemStack deliciousmealitem = new ItemStack(Material.COOKED_BEEF, 10);
		deliciousmealitem = CustomMeta.setMeta(deliciousmealitem, "normal");
		ItemMeta meta = deliciousmealitem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Delicious Meal");
		deliciousmealitem.setItemMeta(meta);
		return deliciousmealitem;
	}

	public static ItemStack evestemptation() {
		ItemStack evestemptationitem = new ItemStack(Material.APPLE, 2);
		evestemptationitem = CustomMeta.setMeta(evestemptationitem, "normal");
		ItemMeta meta = evestemptationitem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Eve's Temptation");
		evestemptationitem.setItemMeta(meta);
		return evestemptationitem;
	}

	public static ItemStack healingfruit() {
		ItemStack healingfruititem = new ItemStack(Material.MELON, 1);
		healingfruititem = CustomMeta.setMeta(healingfruititem, "normal");
		ItemMeta meta = healingfruititem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Healing Fruit");
		healingfruititem.setItemMeta(meta);
		return healingfruititem;
	}

	public static ItemStack enlighteningpack() {
		ItemStack enlighteningpackitem = new ItemStack(Material.EXP_BOTTLE, 8);
		enlighteningpackitem = CustomMeta.setMeta(enlighteningpackitem, "normal");
		ItemMeta meta = enlighteningpackitem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Enlightening Pack");
		enlighteningpackitem.setItemMeta(meta);
		return enlighteningpackitem;
	}

	public static ItemStack lightenchanttable() {
		ItemStack lightenchantitem = new ItemStack(Material.ENCHANTMENT_TABLE);
		lightenchantitem = CustomMeta.setMeta(lightenchantitem, "normal");
		ItemMeta meta = lightenchantitem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Light Enchant Table");
		lightenchantitem.setItemMeta(meta);
		return lightenchantitem;
	}

	public static ItemStack sugarrush() {
		ItemStack sugarrushitem = new ItemStack(Material.SUGAR_CANE, 4);
		sugarrushitem = CustomMeta.setMeta(sugarrushitem, "normal");
		ItemMeta meta = sugarrushitem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Sugar Rush");
		sugarrushitem.setItemMeta(meta);
		return sugarrushitem;
	}

	public static ItemStack expertsseal() {
		ItemStack expertSeal = new ItemStack(Material.NETHER_STAR);
		expertSeal = CustomMeta.setMeta(expertSeal, "ultimate");
		ItemMeta im = expertSeal.getItemMeta();
		im.setDisplayName("�aExpert Seal");
		im.setLore(Arrays.asList("�9Favor", "�7Upon right clicking with this legendary item,",
				"�7every item in your inventory with enchantments ",
				"�7will have all it's enchantments raised by 1 level."));
		expertSeal.setItemMeta(im);
		return expertSeal;
	}

	public static ShapedRecipe gheadrecipe() {
		ItemStack ghead = RecipesList.ghead();
		ShapedRecipe gheadrecipe = new ShapedRecipe(ghead);
		gheadrecipe.shape("GGG", "GHG", "GGG");
		gheadrecipe.setIngredient('G', Material.GOLD_INGOT);
		gheadrecipe.setIngredient('H', heads().getData());
		Bukkit.addRecipe(gheadrecipe);
		return gheadrecipe;

	}

	static ItemStack puff = new ItemStack(Material.RAW_FISH, 1, (short) 3);
	static ItemStack BONE_MEAL = new ItemStack(Material.INK_SACK, 1, (short) 15);

	public static ShapedRecipe dragonswordrecipe() {
		ShapedRecipe dragonswordrecipe = new ShapedRecipe(DragonSword());
		dragonswordrecipe.shape(" M ", " D ", "OMO");
		dragonswordrecipe.setIngredient('M', Material.MAGMA_CREAM);
		dragonswordrecipe.setIngredient('D', Material.DIAMOND_SWORD);
		dragonswordrecipe.setIngredient('O', Material.OBSIDIAN);
		Bukkit.addRecipe(dragonswordrecipe);
		return dragonswordrecipe;
	}

	public static ShapedRecipe quickpickrecipe() {
		ItemStack quickpick = RecipesList.QuickPick();
		ShapedRecipe quickpickrecipe = new ShapedRecipe(quickpick);
		quickpickrecipe.shape("III", "CSC", " S ");
		quickpickrecipe.setIngredient('I', Material.IRON_ORE);
		quickpickrecipe.setIngredient('C', Material.COAL);
		quickpickrecipe.setIngredient('S', Material.STICK);
		Bukkit.addRecipe(quickpickrecipe);
		return quickpickrecipe;
	}

	public static ShapedRecipe ironpackrecipe() {
		ItemStack ironpack = new ItemStack(RecipesList.IronPack());
		ShapedRecipe ironpackrecipe = new ShapedRecipe(ironpack);
		ironpackrecipe.shape("III", "ICI", "III");
		ironpackrecipe.setIngredient('I', Material.IRON_ORE);
		ironpackrecipe.setIngredient('C', Material.COAL);
		Bukkit.addRecipe(ironpackrecipe);
		return ironpackrecipe;
	}

	public static ShapedRecipe sevenleaguerecipe() {
		ItemStack sevenleague = RecipesList.SevenLeagueBoots();
		ShapedRecipe sevenleaguerecipe = new ShapedRecipe(sevenleague);
		sevenleaguerecipe.shape("FEF", "FBF", "FWF");
		sevenleaguerecipe.setIngredient('F', Material.FEATHER);
		sevenleaguerecipe.setIngredient('E', Material.ENDER_PEARL);
		sevenleaguerecipe.setIngredient('B', Material.DIAMOND_BOOTS);
		sevenleaguerecipe.setIngredient('W', Material.WATER_BUCKET);
		Bukkit.addRecipe(sevenleaguerecipe);
		return sevenleaguerecipe;

	}

	public static ShapedRecipe cupidsbowrecipe() {
		ItemStack cupidsbow = RecipesList.CupidsBow();
		ShapedRecipe cupidsbowrecipe = new ShapedRecipe(cupidsbow);
		cupidsbowrecipe.shape(" R ", "HBH", " L ");
		cupidsbowrecipe.setIngredient('H', heads().getData());
		cupidsbowrecipe.setIngredient('R', Material.BLAZE_ROD);
		cupidsbowrecipe.setIngredient('B', Material.BOW);
		cupidsbowrecipe.setIngredient('L', Material.LAVA_BUCKET);
		Bukkit.addRecipe(cupidsbowrecipe);
		return cupidsbowrecipe;
	}

	public static ShapedRecipe bookofsharpeningrecipe() {
		ItemStack bookofsharpening = RecipesList.bos();
		ShapedRecipe bookofsharpeningrecipe = new ShapedRecipe(bookofsharpening);
		bookofsharpeningrecipe.shape("G  ", " PP", " PI");
		bookofsharpeningrecipe.setIngredient('G', Material.FLINT);
		bookofsharpeningrecipe.setIngredient('P', Material.PAPER);
		bookofsharpeningrecipe.setIngredient('I', Material.IRON_SWORD);
		Bukkit.addRecipe(bookofsharpeningrecipe);
		return bookofsharpeningrecipe;
	}

	public static ShapedRecipe bookofpowerrecipe() {
		ItemStack bookofpower = RecipesList.bopower();
		ShapedRecipe bookofpowerrecipe = new ShapedRecipe(bookofpower);
		bookofpowerrecipe.shape("G  ", " PP", " PI");
		bookofpowerrecipe.setIngredient('G', Material.FLINT);
		bookofpowerrecipe.setIngredient('P', Material.PAPER);
		bookofpowerrecipe.setIngredient('I', Material.BONE);
		Bukkit.addRecipe(bookofpowerrecipe);
		return bookofpowerrecipe;
	}

	public static ShapedRecipe bookofprotectionrecipe() {
		ItemStack bookofprotection = RecipesList.bop();
		ShapedRecipe bookofprotectionrecipe = new ShapedRecipe(bookofprotection);
		bookofprotectionrecipe.shape("   ", " PP", " PI");
		bookofprotectionrecipe.setIngredient('P', Material.PAPER);
		bookofprotectionrecipe.setIngredient('I', Material.IRON_INGOT);
		Bukkit.addRecipe(bookofprotectionrecipe);
		return bookofprotectionrecipe;
	}

	public static ShapedRecipe vorpalswordrecipe() {
		ShapedRecipe vorpalswordrecipe = new ShapedRecipe(RecipesList.VorpalSword());
		vorpalswordrecipe.shape(" B ", " S ", " R ");
		vorpalswordrecipe.setIngredient('B', Material.BONE);
		vorpalswordrecipe.setIngredient('S', Material.IRON_SWORD);
		vorpalswordrecipe.setIngredient('R', Material.ROTTEN_FLESH);
		Bukkit.addRecipe(vorpalswordrecipe);
		return vorpalswordrecipe;
	}

	public static ShapedRecipe andurilrecipe() {
		ShapedRecipe andurilrecipe = new ShapedRecipe(RecipesList.Anduril());
		andurilrecipe.shape("FBF", "FBF", "FRF");
		andurilrecipe.setIngredient('F', Material.FEATHER);
		andurilrecipe.setIngredient('B', Material.IRON_BLOCK);
		andurilrecipe.setIngredient('R', Material.BLAZE_ROD);
		Bukkit.addRecipe(andurilrecipe);
		return andurilrecipe;

	}

	public static ShapedRecipe dragonchestplaterecipe() {
		ShapedRecipe dragonchestplaterecipe = new ShapedRecipe(RecipesList.dragonchestplate());
		dragonchestplaterecipe.shape(" M ", " D ", "OAO");
		dragonchestplaterecipe.setIngredient('M', Material.MAGMA_CREAM);
		dragonchestplaterecipe.setIngredient('D', Material.DIAMOND_CHESTPLATE);
		dragonchestplaterecipe.setIngredient('O', Material.OBSIDIAN);
		dragonchestplaterecipe.setIngredient('A', Material.ANVIL);
		Bukkit.addRecipe(dragonchestplaterecipe);
		return dragonchestplaterecipe;
	}

	public static ShapelessRecipe fusionitemrecipe() {
		ShapelessRecipe fusionitemrecipe = new ShapelessRecipe(RecipesList.fusioncraftitem());
		fusionitemrecipe.addIngredient(Material.DIAMOND_CHESTPLATE);
		fusionitemrecipe.addIngredient(Material.DIAMOND_BOOTS);
		fusionitemrecipe.addIngredient(Material.DIAMOND_LEGGINGS);
		fusionitemrecipe.addIngredient(Material.DIAMOND_HELMET);
		Bukkit.addRecipe(fusionitemrecipe);
		return fusionitemrecipe;
	}

	public static ShapedRecipe goldpackrecipe() {
		ShapedRecipe goldpackrecipe = new ShapedRecipe(RecipesList.goldpack());
		goldpackrecipe.shape("III", "ICI", "III");
		goldpackrecipe.setIngredient('I', Material.GOLD_ORE);
		goldpackrecipe.setIngredient('C', Material.COAL);
		Bukkit.addRecipe(goldpackrecipe);
		return goldpackrecipe;
	}

	public static ShapedRecipe philosrecipe() {
		ShapedRecipe philosrecipe = new ShapedRecipe(RecipesList.philos());
		philosrecipe.shape("IGI", "LSL", " S ");
		philosrecipe.setIngredient('I', Material.IRON_ORE);
		philosrecipe.setIngredient('G', Material.GOLD_ORE);
		philosrecipe.setIngredient('L', Material.LAPIS_BLOCK);
		philosrecipe.setIngredient('S', Material.STICK);
		Bukkit.addRecipe(philosrecipe);
		return philosrecipe;
	}

	public static ShapedRecipe exodusrecipe() {
		ShapedRecipe exodus = new ShapedRecipe(RecipesList.exodus());
		exodus.shape("DDD", "DHD", "ECE");
		exodus.setIngredient('D', Material.DIAMOND);
		exodus.setIngredient('H',

				heads().getData());
		exodus.setIngredient('C', Material.GOLDEN_CARROT);
		exodus.setIngredient('E', Material.EMERALD);
		Bukkit.addRecipe(exodus);
		return exodus;
	}

	public static ShapedRecipe saddlerecipe() {
		ShapedRecipe saddlerecipe = new ShapedRecipe(RecipesList.saddle());
		saddlerecipe.shape("LLL", "SLS", "I I");
		saddlerecipe.setIngredient('L', Material.LEATHER);
		saddlerecipe.setIngredient('S', Material.STRING);
		saddlerecipe.setIngredient('I', Material.IRON_INGOT);
		Bukkit.addRecipe(saddlerecipe);
		return saddlerecipe;
	}

	public static ShapedRecipe tarnhelmrecipe() {
		ShapedRecipe tarnhelmrecipe = new ShapedRecipe(RecipesList.tarnhelm());
		tarnhelmrecipe.shape("DID", "DRD", "   ");
		tarnhelmrecipe.setIngredient('D', Material.DIAMOND);
		tarnhelmrecipe.setIngredient('I', Material.IRON_INGOT);
		tarnhelmrecipe.setIngredient('R', Material.REDSTONE_BLOCK);
		Bukkit.addRecipe(tarnhelmrecipe);
		return tarnhelmrecipe;
	}

	public static ShapedRecipe tabletrecipe() {
		ShapedRecipe tablet = new ShapedRecipe(RecipesList.tabletofdestiny());
		tablet.shape(" M ", "SQB", "EEE");
		tablet.setIngredient('M', Material.MAGMA_CREAM);
		tablet.setIngredient('S', Material.DIAMOND_SWORD);
		tablet.setIngredient('Q', Material.BOOK_AND_QUILL);
		tablet.setIngredient('B', Material.BOW);
		tablet.setIngredient('E', Material.EXP_BOTTLE);
		Bukkit.addRecipe(tablet);
		return tablet;
	}

	public static ShapedRecipe sugarrushrecipe() {
		ItemStack cane = new ItemStack(sugarrush());
		ShapedRecipe sugar = new ShapedRecipe(cane);
		sugar.shape(" S ", "DGD", "   ");
		sugar.setIngredient('S', Material.SAPLING);
		sugar.setIngredient('D', Material.SEEDS);
		sugar.setIngredient('G', Material.SUGAR);
		Bukkit.addRecipe(sugar);
		return sugar;
	}

	public static ShapedRecipe spikedarmorecipe() {
		ShapedRecipe spikedrecipe = new ShapedRecipe(RecipesList.spikedarmor());
		spikedrecipe.shape(" L ", " C ", " A ");
		spikedrecipe.setIngredient('L', Material.WATER_LILY);
		spikedrecipe.setIngredient('C', Material.CACTUS);
		spikedrecipe.setIngredient('A', Material.LEATHER_CHESTPLATE);
		Bukkit.addRecipe(spikedrecipe);
		return spikedrecipe;
	}

	public static ShapedRecipe arrowecorecipe() {
		ItemStack arroweco = new ItemStack(RecipesList.arroweco());
		ShapedRecipe arrowecorecipe = new ShapedRecipe(arroweco);
		arrowecorecipe.shape("FFF", "SSS", "EEE");
		arrowecorecipe.setIngredient('F', Material.FLINT);
		arrowecorecipe.setIngredient('S', Material.STICK);
		arrowecorecipe.setIngredient('E', Material.FEATHER);
		Bukkit.addRecipe(arrowecorecipe);
		return arrowecorecipe;
	}

	public static ShapedRecipe lightanvilrecipe() {
		ItemStack lightanvil = new ItemStack(RecipesList.lightanvil());
		ShapedRecipe lightanvilrecipe = new ShapedRecipe(lightanvil);
		lightanvilrecipe.shape("III", " B ", "III");
		lightanvilrecipe.setIngredient('I', Material.IRON_INGOT);
		lightanvilrecipe.setIngredient('B', Material.IRON_BLOCK);
		Bukkit.addRecipe(lightanvilrecipe);
		return lightanvilrecipe;
	}

	public static ShapedRecipe leatherecorecipe() {
		ItemStack leathereco = new ItemStack(Material.LEATHER, 8);
		ShapedRecipe leatherecorecipe = new ShapedRecipe(leathereco);
		leatherecorecipe.shape("SLS", "SLS", "SLS");
		leatherecorecipe.setIngredient('S', Material.STICK);
		leatherecorecipe.setIngredient('L', Material.LEATHER);
		Bukkit.addRecipe(leatherecorecipe);
		return leatherecorecipe;
	}

	public static ShapedRecipe artemisbookrecipe() {
		ShapedRecipe artemisbookrecipe = new ShapedRecipe(artbook());
		artemisbookrecipe.shape("   ", " BB", " BA");
		artemisbookrecipe.setIngredient('B', Material.PAPER);
		artemisbookrecipe.setIngredient('A', Material.ARROW);
		Bukkit.addRecipe(artemisbookrecipe);
		return artemisbookrecipe;
	}

	public static ShapedRecipe vidarrecipe() {
		ShapedRecipe vidarrecipe = new ShapedRecipe(vidar());
		vidarrecipe.shape(" P ", "WBW", " F ");
		vidarrecipe.setIngredient('P', puff.getData());
		vidarrecipe.setIngredient('W', Material.POTION);
		vidarrecipe.setIngredient('B', Material.DIAMOND_BOOTS);
		vidarrecipe.setIngredient('F', Material.FISHING_ROD);
		Bukkit.addRecipe(vidarrecipe);
		return vidarrecipe;
	}

	public static ShapedRecipe dustoflightrecipe() {
		ItemStack glowstone = new ItemStack(dustoflight());
		ShapedRecipe glowstonerecipe = new ShapedRecipe(glowstone);
		glowstonerecipe.shape("RRR", "RFR", "RRR");
		glowstonerecipe.setIngredient('R', Material.REDSTONE);
		glowstonerecipe.setIngredient('F', Material.FLINT_AND_STEEL);
		Bukkit.addRecipe(glowstonerecipe);
		return glowstonerecipe;
	}

	public static ShapedRecipe brewingartrecipe() {
		ItemStack brewingart = new ItemStack(brewingartifact());
		ShapedRecipe brewingartrecipe = new ShapedRecipe(brewingart);
		brewingartrecipe.shape(" S ", "SFS", " S ");
		brewingartrecipe.setIngredient('S', Material.SEEDS);
		brewingartrecipe.setIngredient('F', Material.FERMENTED_SPIDER_EYE);
		Bukkit.addRecipe(brewingartrecipe);
		return brewingartrecipe;
	}

	public static ShapedRecipe netherartifactrecipe() {
		ItemStack blzerod = new ItemStack(netherartifact());
		ShapedRecipe blazerodrecipe = new ShapedRecipe(blzerod);
		blazerodrecipe.shape("SLS", "SFS", "SLS");
		ItemStack stainedorange = new ItemStack(Material.STAINED_GLASS, 1, (byte) 1);
		blazerodrecipe.setIngredient('S', stainedorange.getData());
		blazerodrecipe.setIngredient('F', Material.FIREWORK);
		blazerodrecipe.setIngredient('L', Material.LAVA_BUCKET);
		Bukkit.addRecipe(blazerodrecipe);
		return blazerodrecipe;
	}

	public static ShapedRecipe deliciousmealrecipe() {
		ItemStack steaksitem = new ItemStack(deliciousmeal());
		ShapedRecipe steaks = new ShapedRecipe(steaksitem);
		steaks.shape("RRR", "RCR", "RRR");
		steaks.setIngredient('R', Material.RAW_BEEF);
		steaks.setIngredient('C', Material.COAL);
		Bukkit.addRecipe(steaks);
		return steaks;
	}

	public static ShapedRecipe obbyrecipe() {
		ItemStack obby = new ItemStack(Material.OBSIDIAN);
		ShapelessRecipe obbyrecipe = new ShapelessRecipe(obby);
		obbyrecipe.addIngredient(Material.WATER_BUCKET);
		obbyrecipe.addIngredient(Material.LAVA_BUCKET);
		Bukkit.addRecipe(obbyrecipe);
		return null;
	}

	public static ShapedRecipe evestemptationrecipe() {
		ItemStack apple2 = new ItemStack(Material.APPLE, 2);

		ShapelessRecipe applerecipe = new ShapelessRecipe(apple2);
		applerecipe.addIngredient(Material.APPLE);
		applerecipe.addIngredient(BONE_MEAL.getData());
		Bukkit.addRecipe(applerecipe);
		return null;
	}

	public static ShapedRecipe healingfruitrecipe() {
		ItemStack healingfruit = new ItemStack(Material.MELON);
		ShapedRecipe healingfruitrecipe = new ShapedRecipe(healingfruit);
		healingfruitrecipe.shape("BSB", "SAS", "BSB");
		healingfruitrecipe.setIngredient('B', BONE_MEAL.getData());
		healingfruitrecipe.setIngredient('S', Material.SEEDS);
		healingfruitrecipe.setIngredient('A', Material.APPLE);
		Bukkit.addRecipe(healingfruitrecipe);
		return healingfruitrecipe;
	}

	public static ShapedRecipe lightapplerecipe() {
		ItemStack lightapple = new ItemStack(Material.GOLDEN_APPLE);
		ShapedRecipe lightapplerecipe = new ShapedRecipe(lightapple);
		lightapplerecipe.shape(" G ", "GAG", " G ");
		lightapplerecipe.setIngredient('G', Material.GOLD_INGOT);
		lightapplerecipe.setIngredient('A', Material.APPLE);
		Bukkit.addRecipe(lightapplerecipe);
		return lightapplerecipe;
	}

	public static ShapedRecipe enlighteningpackrecipe() {
		ItemStack enlighteningpack = new ItemStack(Material.EXP_BOTTLE, 8);
		ShapedRecipe enlighteningrecipe = new ShapedRecipe(enlighteningpack);
		enlighteningrecipe.shape(" R ", "RBR", " R ");
		enlighteningrecipe.setIngredient('R', Material.REDSTONE_BLOCK);
		enlighteningrecipe.setIngredient('B', Material.GLASS_BOTTLE);
		Bukkit.addRecipe(enlighteningrecipe);
		return enlighteningrecipe;
	}

	public static ShapedRecipe lightenchanttablerecipe() {
		ItemStack echanttable = new ItemStack(Material.ENCHANTMENT_TABLE);
		ShapedRecipe echantrecipe = new ShapedRecipe(echanttable);
		echantrecipe.shape(" B ", "ODO", "OEO");
		echantrecipe.setIngredient('B', Material.BOOKSHELF);
		echantrecipe.setIngredient('D', Material.DIAMOND);
		echantrecipe.setIngredient('E', Material.OBSIDIAN);
		echantrecipe.setIngredient('E', Material.EXP_BOTTLE);
		Bukkit.addRecipe(echantrecipe);
		return echantrecipe;

	}

	public static ShapedRecipe bookofthothrecipe() {
		ShapedRecipe bookofthothrecipe = new ShapedRecipe(bookofthoth());
		bookofthothrecipe.shape("E  ", " PP", " PF");
		bookofthothrecipe.setIngredient('E', Material.EYE_OF_ENDER);
		bookofthothrecipe.setIngredient('P', Material.PAPER);
		bookofthothrecipe.setIngredient('F', Material.FIREBALL);
		Bukkit.addRecipe(bookofthothrecipe);
		return bookofthothrecipe;
	}

	public static ShapedRecipe fenrirrecipe() {
		ShapedRecipe fenrirrecipe = new ShapedRecipe(fenrir());
		fenrirrecipe.shape("LLL", "BEB", "LLL");
		fenrirrecipe.setIngredient('L', Material.LEATHER);
		fenrirrecipe.setIngredient('B', Material.BONE);
		fenrirrecipe.setIngredient('E', Material.EXP_BOTTLE);
		Bukkit.addRecipe(fenrirrecipe);
		return fenrirrecipe;

	}

	public static ShapedRecipe hiderecipe() {
		ShapedRecipe hiderecipe = new ShapedRecipe(hideofl());
		hiderecipe.shape("LWL", "DdD", "P P");
		hiderecipe.setIngredient('L', Material.LAPIS_BLOCK);
		hiderecipe.setIngredient('W', Material.WATER_BUCKET);
		hiderecipe.setIngredient('d', Material.DIAMOND_LEGGINGS);
		hiderecipe.setIngredient('D', Material.DIAMOND);
		hiderecipe.setIngredient('P', Material.WATER_LILY);
		Bukkit.addRecipe(hiderecipe);
		return hiderecipe;

	}

	public static ShapedRecipe perunrecipe() {
		// Perun
		ShapedRecipe perunrecipe = new ShapedRecipe(axeofperun());
		perunrecipe.shape("DTF", "DS ", " S ");
		perunrecipe.setIngredient('D', Material.DIAMOND);
		perunrecipe.setIngredient('T', Material.TNT);
		perunrecipe.setIngredient('F', Material.FIREBALL);
		perunrecipe.setIngredient('S', Material.STICK);
		Bukkit.addRecipe(perunrecipe);
		return perunrecipe;
	}

	public static ShapedRecipe chestoffaterecipe() {
		ShapedRecipe chestoffaterecipe = new ShapedRecipe(chestoffate());
		chestoffaterecipe.shape("WWW", "WHW", "WWW");
		chestoffaterecipe.setIngredient('W', Material.WOOD);
		chestoffaterecipe.setIngredient('H',

				heads().getData());
		Bukkit.addRecipe(chestoffaterecipe);
		return chestoffaterecipe;
	}

	public static ShapedRecipe nectarrecipe() {
		ShapedRecipe nectarrecipe = new ShapedRecipe(nectar());
		nectarrecipe.shape(" E ", "GMG", " B ");
		nectarrecipe.setIngredient('E', Material.EMERALD);
		nectarrecipe.setIngredient('G', Material.GOLD_INGOT);
		nectarrecipe.setIngredient('M', Material.MELON);
		nectarrecipe.setIngredient('B', Material.GLASS_BOTTLE);
		Bukkit.addRecipe(nectarrecipe);
		return nectarrecipe;
	}

	public static ShapedRecipe toughnessrecipe() {
		ShapedRecipe toughnessrecipe = new ShapedRecipe(potionoftoughness());
		toughnessrecipe.shape(" S ", " I ", " B ");
		toughnessrecipe.setIngredient('S', Material.SLIME_BALL);
		toughnessrecipe.setIngredient('I', Material.IRON_BLOCK);
		toughnessrecipe.setIngredient('B', Material.GLASS_BOTTLE);
		Bukkit.addRecipe(toughnessrecipe);
		return toughnessrecipe;
	}

	public static ShapedRecipe artemisrecipe() {
		ShapedRecipe artemisrecipe = new ShapedRecipe(artmeis());
		artemisrecipe.shape("FDF", "FBF", "FEF");
		artemisrecipe.setIngredient('D', Material.DIAMOND);
		artemisrecipe.setIngredient('B', Material.BOW);
		artemisrecipe.setIngredient('F', Material.FEATHER);
		artemisrecipe.setIngredient('E', Material.EYE_OF_ENDER);
		Bukkit.addRecipe(artemisrecipe);
		return artemisrecipe;
	}

	public static ShapedRecipe pboxrecipe() {
		ShapedRecipe pboxrecipe = new ShapedRecipe(pandorasbox());
		pboxrecipe.shape("CCC", "CHC", "CCC");
		pboxrecipe.setIngredient('C', Material.CHEST);
		pboxrecipe.setIngredient('H',

				heads().getData());
		Bukkit.addRecipe(pboxrecipe);
		return pboxrecipe;
	}

	public static ShapedRecipe flaskrecipe() {
		ShapedRecipe flaskrecipe = new ShapedRecipe(pandorasbox());
		flaskrecipe.shape(" H ", "SGS", " Q ");
		flaskrecipe.setIngredient('S', Material.BROWN_MUSHROOM);
		flaskrecipe.setIngredient('H',

				heads().getData());
		flaskrecipe.setIngredient('Q', Material.INK_SACK);
		Bukkit.addRecipe(flaskrecipe);
		return flaskrecipe;
	}

	public static ShapedRecipe excaliburrecipe() {
		ShapedRecipe excaliburrecipe = new ShapedRecipe(excalibur());
		excaliburrecipe.shape("SFS", "STS", "SDS");
		excaliburrecipe.setIngredient('S', Material.SOUL_SAND);
		excaliburrecipe.setIngredient('F', Material.FIREWORK_CHARGE);
		excaliburrecipe.setIngredient('D', Material.DIAMOND_SWORD);
		Bukkit.addRecipe(excaliburrecipe);
		return excaliburrecipe;
	}

	public static ShapedRecipe daredevilrecipe() {
		ShapedRecipe daredevilrecipe = new ShapedRecipe(daredevil());
		daredevilrecipe.shape("HS ", "BBB", "B B");
		daredevilrecipe.setIngredient('H',

				heads().getData());
		daredevilrecipe.setIngredient('S', Material.SADDLE);
		daredevilrecipe.setIngredient('B', Material.BONE);
		Bukkit.addRecipe(daredevilrecipe);
		return daredevilrecipe;
	}

	public static ShapedRecipe deathsytherecipe() {
		ShapedRecipe deathsytherecipe = new ShapedRecipe(deathsythe());
		deathsytherecipe.shape(" HH", " BC", "B  ");
		deathsytherecipe.setIngredient('H',

				heads().getData());
		deathsytherecipe.setIngredient('B', Material.BONE);
		deathsytherecipe.setIngredient('C', Material.WATCH);
		Bukkit.addRecipe(deathsytherecipe);
		return deathsytherecipe;
	}

	public static ShapedRecipe hermesrecipe() {
		ShapedRecipe hermesrecipe = new ShapedRecipe(hermesboots());
		hermesrecipe.shape("DHD", "LBL", "F F");
		hermesrecipe.setIngredient('D', Material.DIAMOND);
		hermesrecipe.setIngredient('H',

				heads().getData());
		hermesrecipe.setIngredient('L', Material.BLAZE_POWDER);
		hermesrecipe.setIngredient('B', Material.DIAMOND_BOOTS);
		hermesrecipe.setIngredient('F', Material.FEATHER);
		Bukkit.addRecipe(hermesrecipe);
		return hermesrecipe;
	}

	public static ShapedRecipe barbarianchestplaterecipe() {
		ShapedRecipe barbrecipe = new ShapedRecipe(barbarian());
		barbrecipe.shape("   ", "BCB", "ISI");
		barbrecipe.setIngredient('B', Material.BLAZE_ROD);
		barbrecipe.setIngredient('C', Material.DIAMOND_CHESTPLATE);
		barbrecipe.setIngredient('I', Material.IRON_BLOCK);
		ItemStack stpot = new ItemStack(Material.POTION, 1, (short) 8201);
		barbrecipe.setIngredient('S', stpot.getData());
		Bukkit.addRecipe(barbrecipe);
		return barbrecipe;
	}

	public static ShapedRecipe cornrecipe() {
		ShapedRecipe cornrecipe = new ShapedRecipe(corn());
		cornrecipe.shape("CCC", "CGC", "CCC");
		cornrecipe.setIngredient('C', Material.CARROT);
		cornrecipe.setIngredient('G', Material.GOLDEN_APPLE);
		Bukkit.addRecipe(cornrecipe);
		return cornrecipe;
	}

	public static ShapedRecipe kingrodrecipe() {
		ShapedRecipe kingsrodrecipe = new ShapedRecipe(kingsrod());
		kingsrodrecipe.shape(" F ", "LCL", " W ");
		kingsrodrecipe.setIngredient('F', Material.FISHING_ROD);
		kingsrodrecipe.setIngredient('L', Material.WATER_LILY);
		kingsrodrecipe.setIngredient('W', Material.WATER_BUCKET);
		kingsrodrecipe.setIngredient('C', Material.COMPASS);
		Bukkit.addRecipe(kingsrodrecipe);
		return kingsrodrecipe;
	}

	public static ShapedRecipe diceofgodrecipe() {
		ShapedRecipe diceofgodr = new ShapedRecipe(diceofgod());
		diceofgodr.shape("MHM", "MJM", "MMM");
		diceofgodr.setIngredient('M', Material.MOSSY_COBBLESTONE);
		diceofgodr.setIngredient('H',

				heads().getData());
		diceofgodr.setIngredient('J', Material.JUKEBOX);
		Bukkit.addRecipe(diceofgodr);
		return diceofgodr;
	}

	public static ShapedRecipe expertssealrecipe() {
		ShapedRecipe expertsealr = new ShapedRecipe(expertsseal());
		expertsealr.shape("BIB", "GDG", "BIB");

		expertsealr.setIngredient('B', Material.EXP_BOTTLE);
		expertsealr.setIngredient('I', Material.IRON_BLOCK);
		expertsealr.setIngredient('G', Material.GOLD_BLOCK);
		expertsealr.setIngredient('D', Material.DIAMOND_BLOCK);

		Bukkit.addRecipe(expertsealr);
		return expertsealr;
	}

	public static void RegisterRecipes() {

		FileConfiguration config = UHC.getPlugin(UHC.class).getConfig();

		if (config.getBoolean("CustomItems") == true) {

			RecipesList.vorpalswordrecipe();
			RecipesList.bookofsharpeningrecipe();
			RecipesList.bookofprotectionrecipe();
			RecipesList.gheadrecipe();
			RecipesList.dragonswordrecipe();
			RecipesList.andurilrecipe();
			RecipesList.quickpickrecipe();
			RecipesList.sevenleaguerecipe();
			RecipesList.cupidsbowrecipe();
			RecipesList.ironpackrecipe();
			RecipesList.dragonchestplaterecipe();
			RecipesList.goldpackrecipe();
			RecipesList.philosrecipe();
			RecipesList.exodusrecipe();
			RecipesList.saddlerecipe();
			RecipesList.tarnhelmrecipe();
			RecipesList.tabletrecipe();
			RecipesList.spikedarmorecipe();
			RecipesList.vidarrecipe();
			RecipesList.perunrecipe();
			RecipesList.artemisbookrecipe();
			RecipesList.bookofthothrecipe();
			RecipesList.fenrirrecipe();
			RecipesList.hiderecipe();
			RecipesList.nectarrecipe();
			RecipesList.toughnessrecipe();
			RecipesList.lightapplerecipe();
			RecipesList.artemisrecipe();
			RecipesList.pboxrecipe();
			RecipesList.deathsytherecipe();
			RecipesList.sugarrushrecipe();
			RecipesList.arrowecorecipe();
			RecipesList.lightanvilrecipe();
			RecipesList.dustoflightrecipe();
			RecipesList.brewingartrecipe();
			RecipesList.netherartifactrecipe();
			RecipesList.deliciousmealrecipe();
			RecipesList.evestemptationrecipe();
			RecipesList.healingfruitrecipe();
			RecipesList.enlighteningpackrecipe();
			RecipesList.lightenchanttablerecipe();
			RecipesList.hermesrecipe();
			RecipesList.barbarianchestplaterecipe();
			RecipesList.cornrecipe();
			RecipesList.kingrodrecipe();
			RecipesList.daredevilrecipe();
			RecipesList.excaliburrecipe();
			RecipesList.fusionitemrecipe();
			RecipesList.expertssealrecipe();

		}
	}
}
