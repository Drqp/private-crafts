package main.java.Recipes;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class LootBoxes implements Listener {

	@EventHandler
	public void newCraft(PlayerInteractEvent e) {

		List<ItemStack> fusion = new ArrayList<ItemStack>();
		fusion.add(RecipesList.fusionchestplate());
		fusion.add(RecipesList.fusionhelmet());
		fusion.add(RecipesList.fusionboots());
		fusion.add(RecipesList.fusionlegs());

		Random random = new Random();
		int choice = random.nextInt(fusion.size());
		ItemStack fusionpiece = fusion.get(choice);
		ItemStack fusioncraftitem = RecipesList.fusioncraftitem();
		ItemMeta fusionitemmeta = RecipesList.fusioncraftitem().getItemMeta();

		Action action = e.getAction();
		ItemStack item = e.getItem();

		Player p = e.getPlayer();

		if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
			if (item != null && item.getType() == Material.CHEST && item.getItemMeta().equals(fusionitemmeta)) {
				e.setCancelled(true);
				p.getInventory().removeItem(fusioncraftitem);
				p.getInventory().addItem(fusionpiece);
				p.sendMessage(ChatColor.GREEN + "You have recieved a random fusion piece");
			}
		}
	}

	@EventHandler
	public void onCraftPbox(PlayerInteractEvent e) {

		Action action = e.getAction();
		ItemStack item = e.getItem();

		Player p = e.getPlayer();

		List<ItemStack> pbox = new ArrayList<ItemStack>();

		ItemStack instaheal1 = new ItemStack(Material.POTION, 1, (short) 8197);
		PotionMeta instaheal1ameta = (PotionMeta) instaheal1.getItemMeta();
		instaheal1ameta.addCustomEffect(new PotionEffect(PotionEffectType.HEAL, 20, 0), true);
		instaheal1.setItemMeta(instaheal1ameta);
		pbox.add(instaheal1);

		ItemStack instaheal2 = new ItemStack(Material.POTION, 1, (short) 8197);
		PotionMeta instaheal2ameta = (PotionMeta) instaheal2.getItemMeta();
		instaheal2ameta.addCustomEffect(new PotionEffect(PotionEffectType.HEAL, 20, 1), true);
		instaheal2.setItemMeta(instaheal2ameta);
		pbox.add(instaheal2);

		ItemStack instaheal3 = new ItemStack(Material.POTION, 1, (short) 8197);
		PotionMeta instaheal3ameta = (PotionMeta) instaheal3.getItemMeta();
		instaheal3ameta.addCustomEffect(new PotionEffect(PotionEffectType.HEAL, 20, 2), true);
		instaheal3.setItemMeta(instaheal3ameta);
		pbox.add(instaheal3);

		ItemStack instaheal4 = new ItemStack(Material.POTION, 1, (short) 8197);
		PotionMeta instaheal4ameta = (PotionMeta) instaheal4.getItemMeta();
		instaheal4ameta.addCustomEffect(new PotionEffect(PotionEffectType.HEAL, 20, 2), true);
		instaheal4.setItemMeta(instaheal4ameta);
		pbox.add(instaheal4);

		ItemStack gapple1 = new ItemStack(Material.GOLDEN_APPLE, 1);
		pbox.add(gapple1);
		ItemStack gapple2 = new ItemStack(Material.GOLDEN_APPLE, 2);
		pbox.add(gapple2);
		ItemStack gapple3 = new ItemStack(Material.GOLDEN_APPLE, 3);
		pbox.add(gapple3);
		ItemStack gold = new ItemStack(Material.GOLD_INGOT, 8);
		pbox.add(gold);
		ItemStack gold1 = new ItemStack(Material.GOLD_INGOT, 12);
		pbox.add(gold1);
		ItemStack gold2 = new ItemStack(Material.GOLD_INGOT, 16);
		pbox.add(gold2);
		ItemStack gold3 = new ItemStack(Material.GOLD_INGOT, 24);
		pbox.add(gold3);
		ItemStack diamond1 = new ItemStack(Material.DIAMOND, 3);
		pbox.add(diamond1);
		ItemStack diamond2 = new ItemStack(Material.DIAMOND, 5);
		pbox.add(diamond2);
		ItemStack expbottle3 = new ItemStack(Material.EXP_BOTTLE, 24);
		pbox.add(expbottle3);
		ItemStack expbottle1 = new ItemStack(Material.EXP_BOTTLE, 32);
		pbox.add(expbottle1);
		ItemStack expbottle2 = new ItemStack(Material.EXP_BOTTLE, 48);
		pbox.add(expbottle2);
		ItemStack sharpness3 = new ItemStack(Material.ENCHANTED_BOOK);
		sharpness3.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 3);
		pbox.add(sharpness3);
		ItemStack sharpness4 = new ItemStack(Material.ENCHANTED_BOOK);
		sharpness4.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 4);
		pbox.add(sharpness4);
		ItemStack fireaspect1 = new ItemStack(Material.ENCHANTED_BOOK);
		fireaspect1.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 1);
		pbox.add(fireaspect1);
		ItemStack fireaspect2 = new ItemStack(Material.ENCHANTED_BOOK);
		fireaspect2.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 2);
		pbox.add(fireaspect2);
		ItemStack arrow1 = new ItemStack(Material.ENCHANTED_BOOK);
		arrow1.addUnsafeEnchantment(Enchantment.ARROW_FIRE, 1);
		pbox.add(arrow1);
		ItemStack power4 = new ItemStack(Material.ENCHANTED_BOOK);
		arrow1.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 4);
		pbox.add(power4);
		ItemStack pot1item = new ItemStack(Material.POTION, 1, (short) 8197);
		PotionMeta pot1meta = (PotionMeta) pot1item.getItemMeta();
		pot1meta.addCustomEffect(new PotionEffect(PotionEffectType.SPEED, 1200, 1), true);
		pot1meta.addCustomEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1200, 1), true);
		pot1item.setItemMeta(pot1meta);
		pbox.add(pot1item);
		ItemStack pot2item = new ItemStack(Material.POTION, 1, (short) 8197);
		PotionMeta pot2meta = (PotionMeta) pot2item.getItemMeta();
		pot2meta.addCustomEffect(new PotionEffect(PotionEffectType.SPEED, 1200, 1), true);
		pot2meta.addCustomEffect(new PotionEffect(PotionEffectType.ABSORPTION, 1200, 2), true);
		pot2item.setItemMeta(pot2meta);
		pbox.add(pot2item);
		ItemStack pot3item = new ItemStack(Material.POTION, 1, (short) 8197);
		PotionMeta pot3meta = (PotionMeta) pot3item.getItemMeta();
		pot3meta.addCustomEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1200, 1), true);
		pot3meta.addCustomEffect(new PotionEffect(PotionEffectType.HEAL, 40, 2), true);
		pot3item.setItemMeta(pot3meta);
		pbox.add(pot3item);
		ItemStack slapfish = new ItemStack(Material.RAW_FISH);
		ItemMeta slapfishmeta = slapfish.getItemMeta();
		slapfishmeta.setDisplayName(ChatColor.GREEN + "Slapfish");
		slapfishmeta.addEnchant(Enchantment.DAMAGE_ALL, 3, true);
		slapfish.setItemMeta(slapfishmeta);
		pbox.add(slapfish);

		Random random = new Random();
		int choice = random.nextInt(pbox.size());
		ItemStack pboxitem = pbox.get(choice);

		if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
			if (item != null && item.getType() == Material.CHEST
					&& item.getItemMeta().equals(RecipesList.pandorasbox().getItemMeta())) {
				e.setCancelled(true);
				p.getInventory().removeItem(RecipesList.pandorasbox());
				p.getInventory().addItem(pboxitem);
				p.sendMessage(ChatColor.GREEN + "You have recieved a random item");
			}
		}

	}
	
	
	

}
