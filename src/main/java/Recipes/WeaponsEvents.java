package main.java.Recipes;

import static org.bukkit.Material.DIAMOND_AXE;
import static org.bukkit.Material.DIAMOND_SWORD;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import main.java.UHC;
import main.java.Events.Items.ItemsEvents;

public class WeaponsEvents implements Listener {

	private main.java.UHC plugin = UHC.getPlugin(UHC.class);

	@EventHandler
	public void onDamageEvent(EntityDamageByEntityEvent e) {

		ItemMeta exodusmeta = RecipesList.exodus().getItemMeta();

		if (e.getDamager() instanceof Player && e.getEntity() instanceof Player) {

			Player attacker = (Player) e.getDamager();
			Player attacked = (Player) e.getEntity();
			ItemStack helm = attacker.getInventory().getHelmet();

			if (helm != null && helm.getType() == Material.DIAMOND_HELMET) {
				if (attacker.getInventory().getHelmet().getItemMeta().equals(exodusmeta)) {
					attacker.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 80, 0));
				}
			}
		}
	}

	@EventHandler
	public void onPlayerPlace(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (p.getItemInHand().equals(RecipesList.fenrir())) {
				e.setCancelled(true);
				p.getInventory().removeItem(RecipesList.fenrir());
				Wolf wolf = (Wolf) e.getPlayer().getWorld().spawnEntity(p.getLocation(), EntityType.WOLF);
				wolf.setOwner(p);
				wolf.setAdult();
				wolf.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100000, 0));
				wolf.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100000, 1));
				wolf.setCustomName(ChatColor.GREEN + "Fenrir");
			} else if (p.getItemInHand().equals(RecipesList.daredevil())) {
				e.setCancelled(true);
				p.getInventory().removeItem(RecipesList.daredevil());
				Horse horse = (Horse) e.getPlayer().getWorld().spawnEntity(p.getLocation(), EntityType.HORSE);
				horse.setOwner(p);
				horse.setVariant(Horse.Variant.SKELETON_HORSE);
				horse.setAdult();
				horse.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100000, 8));
				horse.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100000, 1));
				ItemStack saddle = new ItemStack(Material.SADDLE);
				horse.getInventory().setSaddle(saddle);
				horse.setCustomName(ChatColor.RED + "DareDevil");
			}
		}
	}

	@EventHandler
	public void onAttackPerun(EntityDamageByEntityEvent e) {

		FileConfiguration config = UHC.plugin.getConfig();

		if (e.getDamager() instanceof Player && e.getEntity() instanceof Player) {

			Player attacker = (Player) e.getDamager();
			Player attacked = (Player) e.getEntity();
			UUID uuid = attacker.getUniqueId();

			if (attacker.getItemInHand().getType() == DIAMOND_AXE) {
				if (attacker.getItemInHand().getItemMeta().equals(RecipesList.axeofperun().getItemMeta())) {
					if (!ItemsEvents.cooldownperun.containsKey(uuid)) {
						attacked.getWorld().strikeLightningEffect(attacked.getLocation());
						attacked.setHealth(attacked.getHealth() - 4);
						attacker.getItemInHand()
								.setDurability((short) ((short) attacker.getItemInHand().getDurability() + 156));
						ItemsEvents.cooldownperun.put(uuid, config.getInt("peruncooldowntime"));
					} else {
						if (ItemsEvents.cooldownmessages.contains(uuid)) {
							attacker.sendMessage(ChatColor.GREEN + "Perun is on cooldown for "
									+ ItemsEvents.cooldownperun.get(uuid) + " seconds.");
						}
					}
				}
			}
		}
	}

	@EventHandler
	public void onAttackExcalibur(EntityDamageByEntityEvent e) {

		if (e.getDamager() instanceof Player && e.getEntity() instanceof Player) {

			Player attacker = (Player) e.getDamager();
			Player attacked = (Player) e.getEntity();
			UUID uuid = attacker.getUniqueId();
			FileConfiguration config = UHC.plugin.getConfig();

			if (attacker.getItemInHand().getType() == DIAMOND_SWORD) {
				if (attacker.getItemInHand().getItemMeta().equals(RecipesList.excalibur().getItemMeta())) {
					if (!ItemsEvents.cooldownexcal.containsKey(uuid)) {
						attacked.getWorld().createExplosion(attacked.getLocation(), 0);
						attacked.setHealth(attacked.getHealth() - 4);
						ItemsEvents.cooldownexcal.put(uuid, config.getInt("excaliburcooldowntime"));
					} else {
						if (ItemsEvents.cooldownmessages.contains(uuid)) {
							attacker.sendMessage(ChatColor.GREEN + "Excalibur is on cooldown for "
									+ ItemsEvents.cooldownexcal.get(uuid) + " seconds.");
						}
					}
				}
			}
		}
	}

	@EventHandler
	public void onAttackSythe(EntityDamageByEntityEvent e) {

		if (e.getDamager() instanceof Player && e.getEntity() instanceof Player) {

			Player attacker = (Player) e.getDamager();
			Player attacked = (Player) e.getEntity();

			if (attacker.getItemInHand().getType() == Material.IRON_HOE) {
				if (attacker.getItemInHand().getItemMeta().equals(RecipesList.deathsythe().getItemMeta())) {
					e.setDamage(attacked.getHealth() * 0.2);
					attacker.setHealth(attacker.getHealth() + (attacked.getHealth() * (5 / 100)));
					attacker.getItemInHand()
							.setDurability((short) ((short) attacker.getItemInHand().getDurability() + 50));

				}
			}
		}
	}

	@EventHandler
	public void onEnchantDrag(EntityDamageByEntityEvent e) {

		Enchantment sharpness = Enchantment.DAMAGE_ALL;

		if (e.getDamager() instanceof Player) {

			Player attacker = (Player) e.getDamager();

			if (attacker.getItemInHand().getType() == DIAMOND_SWORD) {
				if (attacker.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Dragon Sword")) {
					if (attacker.getItemInHand().getEnchantments().containsKey(sharpness)) {
						if (attacker.getItemInHand().getEnchantmentLevel(sharpness) == 1) {
							e.setDamage(9.25);
						} else if (attacker.getItemInHand().getEnchantmentLevel(sharpness) == 2) {
							e.setDamage(10.5);
						} else if (attacker.getItemInHand().getEnchantmentLevel(sharpness) == 3) {
							e.setDamage(11.75);
						} else if (attacker.getItemInHand().getEnchantmentLevel(sharpness) == 4) {
							e.setDamage(13);
						} else if (attacker.getItemInHand().getEnchantmentLevel(sharpness) == 5) {
							e.setDamage(14.25);
						}
					}
				}
			}
		}
	}
}
