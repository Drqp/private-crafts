package main.java.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;

import main.java.Recipes.RecipesList;

public class AllItems {

	public static List<ItemStack> Items = new ArrayList<ItemStack>();

	public static List<ShapedRecipe> Recipes = new ArrayList<ShapedRecipe>();

	public static List<ShapelessRecipe> Shapless = new ArrayList<ShapelessRecipe>();

	public static void allitems() {

		Items.add(RecipesList.VorpalSword());
		Items.add(RecipesList.bos());
		Items.add(RecipesList.bop());
		Items.add(RecipesList.ghead());
		Items.add(RecipesList.DragonSword());
		Items.add(RecipesList.Anduril());
		Items.add(RecipesList.QuickPick());
		Items.add(RecipesList.SevenLeagueBoots());
		Items.add(RecipesList.CupidsBow());
		Items.add(RecipesList.IronPack());
		Items.add(RecipesList.dragonchestplate());
		Items.add(RecipesList.fusioncraftitem());
		Items.add(RecipesList.goldpack());
		Items.add(RecipesList.philos());
		Items.add(RecipesList.exodus());
		Items.add(RecipesList.saddle());
		Items.add(RecipesList.tarnhelm());
		Items.add(RecipesList.tabletofdestiny());
		Items.add(RecipesList.spikedarmor());
		Items.add(RecipesList.vidar());
		Items.add(RecipesList.axeofperun());
		Items.add(RecipesList.artbook());
		Items.add(RecipesList.bookofthoth());
		Items.add(RecipesList.fenrir());
		Items.add(RecipesList.hideofl());
		Items.add(RecipesList.nectar());
		Items.add(RecipesList.potionoftoughness());
		Items.add(RecipesList.holywater());
		Items.add(RecipesList.nextpage());
		Items.add(RecipesList.lightapple());
		Items.add(RecipesList.artmeis());
		Items.add(RecipesList.pandorasbox());
		Items.add(RecipesList.deathsythe());
		Items.add(RecipesList.sugarrush());
		Items.add(RecipesList.arroweco());
		Items.add(RecipesList.lightanvil());
		Items.add(RecipesList.dustoflight());
		Items.add(RecipesList.brewingartifact());
		Items.add(RecipesList.netherartifact());
		Items.add(RecipesList.deliciousmeal());
		Items.add(RecipesList.evestemptation());
		Items.add(RecipesList.healingfruit());
		Items.add(RecipesList.enlighteningpack());
		Items.add(RecipesList.lightenchanttable());
		Items.add(RecipesList.hermesboots());
		Items.add(RecipesList.barbarian());
		Items.add(RecipesList.corn());
		Items.add(RecipesList.kingsrod());
		Items.add(RecipesList.prevpage());
	}

	public static void shapedrecipes() {

		Recipes.add(RecipesList.vorpalswordrecipe());
		Recipes.add(RecipesList.bookofsharpeningrecipe());
		Recipes.add(RecipesList.bookofprotectionrecipe());
		Recipes.add(RecipesList.gheadrecipe());
		Recipes.add(RecipesList.dragonswordrecipe());
		Recipes.add(RecipesList.andurilrecipe());
		Recipes.add(RecipesList.quickpickrecipe());
		Recipes.add(RecipesList.sevenleaguerecipe());
		Recipes.add(RecipesList.cupidsbowrecipe());
		Recipes.add(RecipesList.ironpackrecipe());
		Recipes.add(RecipesList.dragonchestplaterecipe());
		Shapless.add(RecipesList.fusionitemrecipe());
		Recipes.add(RecipesList.goldpackrecipe());
		Recipes.add(RecipesList.philosrecipe());
		Recipes.add(RecipesList.exodusrecipe());
		Recipes.add(RecipesList.saddlerecipe());
		Recipes.add(RecipesList.tarnhelmrecipe());
		Recipes.add(RecipesList.tabletrecipe());
		Recipes.add(RecipesList.spikedarmorecipe());
		Recipes.add(RecipesList.vidarrecipe());
		Recipes.add(RecipesList.perunrecipe());
		Recipes.add(RecipesList.artemisbookrecipe());
		Recipes.add(RecipesList.bookofthothrecipe());
		Recipes.add(RecipesList.fenrirrecipe());
		Recipes.add(RecipesList.hiderecipe());
		Recipes.add(RecipesList.nectarrecipe());
		Recipes.add(RecipesList.toughnessrecipe());
		Recipes.add(RecipesList.lightapplerecipe());
		Recipes.add(RecipesList.artemisrecipe());
		Recipes.add(RecipesList.pboxrecipe());
		Recipes.add(RecipesList.deathsytherecipe());
		Recipes.add(RecipesList.sugarrushrecipe());
		Recipes.add(RecipesList.arrowecorecipe());
		Recipes.add(RecipesList.lightanvilrecipe());
		Recipes.add(RecipesList.dustoflightrecipe());
		Recipes.add(RecipesList.brewingartrecipe());
		Recipes.add(RecipesList.netherartifactrecipe());
		Recipes.add(RecipesList.deliciousmealrecipe());
		Recipes.add(RecipesList.evestemptationrecipe());
		Recipes.add(RecipesList.healingfruitrecipe());
		Recipes.add(RecipesList.enlighteningpackrecipe());
		Recipes.add(RecipesList.lightenchanttablerecipe());
		Recipes.add(RecipesList.hermesrecipe());
		Recipes.add(RecipesList.barbarianchestplaterecipe());
		Recipes.add(RecipesList.cornrecipe());
		Recipes.add(RecipesList.kingrodrecipe());

	}

	public static ShapedRecipe getRecipe(ItemStack i) {

		for (ShapedRecipe r : AllItems.Recipes) {

			try {

				if (i.getItemMeta().getDisplayName() == r.getResult().getItemMeta().getDisplayName()) {

					return r;

				}
			} catch (NullPointerException e) {

				System.out.print("" + i.getItemMeta().getDisplayName());

				System.out.print("" + r.getResult().getItemMeta());
			}

		}

		return null;

	}

}
