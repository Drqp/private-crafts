package main.java.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.libs.jline.internal.InputStreamReader;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import main.java.UHC;

public class UpdateChecker implements Listener {

	static boolean currentsetting = true;

	static Boolean newVersion = true;

	@SuppressWarnings("resource")
	public static void serverstart() {

		FileConfiguration config = UHC.plugin.getConfig();

		if (config.getString("update-check").equals("true")) {
			Bukkit.getConsoleSender()
					.sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.WHITE + "Checking for updates...");
			try {
				HttpURLConnection c = (HttpURLConnection) new URL(
						"https://api.spigotmc.org/legacy/update.php?resource=72777").openConnection();
				String newVersion = new BufferedReader(new InputStreamReader(c.getInputStream())).readLine();
				c.disconnect();
				String oldVersion = UHC.plugin.getDescription().getVersion();
				if ((newVersion.equals(oldVersion))) {
					Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.GREEN
							+ "No updates available. The UHC Champion Plugin is up to date");
				} else {
					Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.YELLOW
							+ "Update available for UHC Champions Plugin!");
					Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.YELLOW
							+ "Download it from: https://www.spigotmc.org/resources/uhc-champions-items-and-crafts-1-8.72777/");
				}
			} catch (IOException ex) {
				Bukkit.getConsoleSender().sendMessage(
						ChatColor.AQUA + "[UHCItems] " + ChatColor.RED + "ERROR: Could not contact with Spigot");
				ex.printStackTrace();
			}
		} else {
			Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.WHITE
					+ "UpdateChecker set to false, no update-checking will be proceed");
		}

		Bukkit.getConsoleSender()
				.sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.WHITE + "Checking for remote commands...");

		try {

			URL u = new URL("https://drqp.s3-us-west-1.amazonaws.com/UHCPlugin.html");
			InputStream ins = u.openStream();
			InputStreamReader isr = new InputStreamReader(ins);
			BufferedReader websiteText = new BufferedReader(isr);
			String inputLine;
			while ((inputLine = websiteText.readLine()) != null) {

				if (inputLine.contains("<body>")) {

					inputLine = inputLine.replace("<body>", "");
					inputLine = inputLine.replace("</body>", "");

					newVersion = Boolean.valueOf(inputLine);

				}
			}

			if ((currentsetting == Boolean.valueOf(newVersion))) {
				Bukkit.getConsoleSender()
						.sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.GREEN + "Plugin has been enabled!");
				UHC.pluginon = true;
			} else {
				UHC.pluginon = false;
			}
		} catch (IOException ex) {
			Bukkit.getConsoleSender().sendMessage(
					ChatColor.AQUA + "[UHCItems] " + ChatColor.RED + "ERROR: Could not contact with Server");
			UHC.pluginon = false;
			ex.printStackTrace();
		}

	}

	@EventHandler
	public void PlayerJoinEvent(PlayerJoinEvent e) {

		FileConfiguration config = UHC.getPlugin(UHC.class).getConfig();

		if (e.getPlayer().isOp()) {

			if (config.getString("update-check").equals("true")) {
				e.getPlayer().sendMessage(ChatColor.WHITE + "Checking for updates...");
				try {
					HttpURLConnection c = (HttpURLConnection) new URL(
							"https://api.spigotmc.org/legacy/update.php?resource=72777").openConnection();
					String newVersion = new BufferedReader(new InputStreamReader(c.getInputStream())).readLine();
					c.disconnect();
					String oldVersion = UHC.plugin.getDescription().getVersion();
					System.out.println(newVersion);
					if ((newVersion.equals(oldVersion))) {
						e.getPlayer().sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.GREEN
								+ "No updates available. The UHC Champion Plugin is up to date");
					} else {
						e.getPlayer().sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.YELLOW
								+ "Update available for UHC Champions Plugin!");
						e.getPlayer().sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.YELLOW
								+ "Download it from: https://www.spigotmc.org/resources/uhc-champions-items-and-crafts-1-8.72777/");
					}
				} catch (IOException ex) {
					e.getPlayer().sendMessage(
							ChatColor.AQUA + "[UHCItems] " + ChatColor.RED + "ERROR: Could not contact with Spigot");
					ex.printStackTrace();
				}
			} else {
				e.getPlayer().sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.WHITE
						+ "UpdateChecker set to false, no update-checking will be proceed");
			}

			if (UHC.pluginon = false) {

				e.getPlayer().sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.RED
						+ "UHC Items is Currently Disabled! Contact the Owner!");

			}
		}
	}
}
