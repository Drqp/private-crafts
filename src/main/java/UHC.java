package main.java;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.plugin.java.JavaPlugin;

import main.java.Commands.ArrowMessageDisable;
import main.java.Commands.ConfigReload;
import main.java.Commands.CoolDownCommand;
import main.java.Commands.GiveCommand;
import main.java.Crafts.CraftLimit;
import main.java.Crafts.LimiterEvent;
import main.java.Events.CutCleanAndTools.OreMineEvent;
import main.java.Events.CutCleanAndTools.PickaxeCraftEvent;
import main.java.Events.Heals.HeadDrop;
import main.java.Events.Items.ItemsEvents;
import main.java.Guis.GuiClick;
import main.java.Guis.ScenariosGui;
import main.java.Guis.Items.RecipesGUI;
import main.java.Guis.newGUISystem.testGUI;
import main.java.Recipes.LootBoxes;
import main.java.Recipes.RecipesList;
import main.java.Recipes.WeaponsEvents;
import main.java.chat.ArrowShot;
import main.java.utils.AllItems;
import main.java.utils.UpdateChecker;

public class UHC extends JavaPlugin implements Listener {

	public static UHC plugin;

	public boolean requireupdate = true;

	public String host, database, username, password, table;
	public int port;

	public static boolean pluginon;

	@Override
	public void onEnable() {

		UHC.plugin = this;

		UpdateChecker.serverstart();

		if (pluginon == true) {

			getServer().getPluginManager().registerEvents(new OreMineEvent(), this);
			getServer().getPluginManager().registerEvents(new PickaxeCraftEvent(), this);
			getServer().getPluginManager().registerEvents(new HeadDrop(), this);
			getServer().getPluginManager().registerEvents(new ScenariosGui(), this);
			getServer().getPluginManager().registerEvents(new WeaponsEvents(), this);
			getServer().getPluginManager().registerEvents(new LootBoxes(), this);
			getServer().getPluginManager().registerEvents(new ArrowShot(), this);
			getServer().getPluginManager().registerEvents(new RecipesGUI(), this);
			getServer().getPluginManager().registerEvents(new GuiClick(), this);
			getServer().getPluginManager().registerEvents(new UpdateChecker(), this);
			getServer().getPluginManager().registerEvents(new ItemsEvents(), this);
			getServer().getPluginManager().registerEvents(new LimiterEvent(), this);
			getServer().getPluginManager().registerEvents(new testGUI(), this);

			this.getCommand("scenarios").setExecutor(new ScenariosGui());
			this.getCommand("uhcreload").setExecutor(new ConfigReload());
			this.getCommand("uhcgive").setExecutor(new GiveCommand());
			this.getCommand("itemcooldown").setExecutor(new CoolDownCommand());
			this.getCommand("recipes").setExecutor(new RecipesGUI());
			this.getCommand("arrowmessages").setExecutor(new ArrowMessageDisable());

			this.getConfig().options().copyDefaults(true);
			this.saveConfig();

			RecipesList.RegisterRecipes();

			ItemsEvents.runnablerunnerperun();
			ItemsEvents.runnablerunnerexcal();
			ItemsEvents.andurilrunnable();

			AllItems.allitems();
			AllItems.shapedrecipes();

			for (ShapedRecipe r : AllItems.Recipes) {
				Bukkit.getConsoleSender().sendMessage("" + r.getResult().getItemMeta().getDisplayName());
			}
			for (ItemStack is : AllItems.Items) {

				Bukkit.getConsoleSender().sendMessage("" + AllItems.getRecipe(is));

			}

			CraftLimit.registerMax();

			Bukkit.getConsoleSender()
					.sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.GREEN + "Registered Crafts!");

			for (Player p : Bukkit.getOnlinePlayers()) {
				CraftLimit.registerPlayer(p);
			}

		} else {

			Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.RED
					+ "UHC Items is Currently Disabled! Contact the Owner!");

		}
	}

	@Override
	public void onDisable() {
		Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.RED + "Disabling!");
		Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[UHCItems] " + ChatColor.RED + "Unregistering Crafts!");

	}

}
