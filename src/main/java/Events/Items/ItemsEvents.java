package main.java.Events.Items;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import main.java.UHC;
import main.java.Recipes.HomingTask;
import main.java.Recipes.RecipesList;

public class ItemsEvents implements Listener {

	public static HashMap<UUID, Integer> cooldownperun = new HashMap<UUID, Integer>();
	public static int peruncooldowntime = UHC.plugin.getConfig().getInt("peruncooldowntime");
	public static HashMap<UUID, Integer> cooldownexcal = new HashMap<UUID, Integer>();
	public static int excalcooldowntime = UHC.plugin.getConfig().getInt("excaliburcooldowntime");
	public static ArrayList<UUID> cooldownmessages = new ArrayList<UUID>();

	@EventHandler
	public void eventArrowFired(EntityShootBowEvent e) {

		ArrayList<Integer> chancelist = new ArrayList<Integer>();
		chancelist.add(1);
		chancelist.add(2);
		chancelist.add(3);
		chancelist.add(4);

		Random random = new Random();
		int choice = random.nextInt(chancelist.size());
		int finalchoice = chancelist.get(choice);

		LivingEntity player = e.getEntity();

		if (finalchoice == 4) {
			if (((e.getEntity() instanceof Player)) && ((e.getProjectile() instanceof Arrow))) {
				if (((HumanEntity) player).getItemInHand().getItemMeta().equals(RecipesList.artmeis().getItemMeta())) {

					double minAngle = 6.283185307179586D;
					Entity minEntity = null;
					for (Entity entity : player.getNearbyEntities(64.0D, 64.0D, 64.0D)) {
						if ((player.hasLineOfSight(entity)) && ((entity instanceof LivingEntity))
								&& (!entity.isDead())) {
							Vector toTarget = entity.getLocation().toVector().clone()
									.subtract(player.getLocation().toVector());
							double angle = e.getProjectile().getVelocity().angle(toTarget);
							if (angle < minAngle) {
								minAngle = angle;
								minEntity = entity;
							}
						}
					}
					if (minEntity != null) {
						new HomingTask((Arrow) e.getProjectile(), (LivingEntity) minEntity, UHC.plugin);
					}
				}
			}
		}
	}

	@EventHandler
	public void doublehealthsetting(PlayerJoinEvent e) {

		FileConfiguration config = UHC.getPlugin(UHC.class).getConfig();

		Player p = e.getPlayer();

		if (config.getBoolean("DoubleHealth") == true) {

			p.setMaxHealth(40);
		} else {
			p.setMaxHealth(20);
		}

	}

	public static void runnablerunnerperun() {
		new BukkitRunnable() {

			@Override
			public void run() {

				if (cooldownperun.isEmpty()) {
					return;
				}
				for (UUID uuid : cooldownperun.keySet()) {
					int timeleftperun = cooldownperun.get(uuid);
					if (timeleftperun <= 0) {
						cooldownperun.remove(uuid);
					} else {
						cooldownperun.put(uuid, timeleftperun - 1);
					}
				}

			}

		}.runTaskTimer(UHC.plugin, 0, 20);
	}

	public static void runnablerunnerexcal() {
		new BukkitRunnable() {

			@Override
			public void run() {

				if (cooldownexcal.isEmpty()) {
					return;
				}
				for (UUID uuid : cooldownexcal.keySet()) {
					int timeleftexcal = cooldownexcal.get(uuid);
					if (timeleftexcal <= 0) {
						cooldownexcal.remove(uuid);
					} else {
						cooldownexcal.put(uuid, timeleftexcal - 1);
					}
				}

			}

		}.runTaskTimer(UHC.plugin, 0, 20);
	}

	public static void andurilrunnable() {
		new BukkitRunnable() {

			@Override
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
					if (p.getItemInHand().getType().equals(Material.IRON_SWORD)) {
						if (p.getItemInHand().getItemMeta().equals(RecipesList.Anduril().getItemMeta())) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 60, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 60, 0));
						}
					}

					if (p.getInventory().getBoots() != null) {
						if (p.getInventory().getBoots().getItemMeta().equals(RecipesList.hermesboots().getItemMeta())) {

							float speed = (float) 0.225;

							p.setWalkSpeed(speed);
						} else {
							p.setWalkSpeed((float) 0.2);
						}
					} else {
						p.setWalkSpeed((float) 0.2);
					}

					if (p.getInventory().getChestplate() != null) {
						if (p.getInventory().getChestplate().getItemMeta()
								.equals(RecipesList.barbarian().getItemMeta())) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 60, 0));
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 60, 0));
						}
					}
				}
			}

		}.runTaskTimer(UHC.plugin, 0, 20);
	}

	@EventHandler
	public void expertevent(PlayerInteractEvent e) {

		Player p = e.getPlayer();
		Action action = e.getAction();
		ItemStack helditem = e.getItem();

		if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {

			if (helditem != null && helditem.getType() == Material.NETHER_STAR
					&& helditem.getItemMeta().equals(RecipesList.expertsseal().getItemMeta())) {

				if (p.getItemInHand().getAmount() == 1) {
					p.setItemInHand(new ItemStack(Material.AIR));
				} else {
					p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
				}

				for (ItemStack item : p.getInventory().getContents()) {
					if (item != null) {
						if (item.getEnchantments().size() > 0) {
							Map<Enchantment, Integer> enchantments = new HashMap<>();

							enchantments.putAll(item.getEnchantments());

							for (Enchantment en : enchantments.keySet()) {

								int oldValue = enchantments.get(en);
								enchantments.replace(en, oldValue + 1);
								item.removeEnchantment(en);
							}
							item.addUnsafeEnchantments(enchantments);
						}
					}
				}

				for (ItemStack item : p.getInventory().getArmorContents()) {
					if (item != null) {
						if (item.getEnchantments().size() > 0) {
							Map<Enchantment, Integer> enchantments = new HashMap<>();

							enchantments.putAll(item.getEnchantments());

							for (Enchantment en : enchantments.keySet()) {

								int oldValue = enchantments.get(en);
								enchantments.replace(en, oldValue + 1);
								item.removeEnchantment(en);
							}
							item.addUnsafeEnchantments(enchantments);
						}
					}
				}
			}
		}
	}

}
