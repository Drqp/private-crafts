package main.java.Events.Heals;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.ItemStack;

import main.java.UHC;

public class EnchantedGoldenApple implements Listener {

	FileConfiguration config = UHC.getPlugin(UHC.class).getConfig();

	@EventHandler
	public void onCraftGapple(CraftItemEvent e) {
		if (config.getBoolean("EnchantedGoldenApples") == false) {

			Player p = (Player) e.getWhoClicked();
			ItemStack godapple = new ItemStack(Material.GOLDEN_APPLE, 1, (short) 1);

			if (e.getRecipe().getResult().equals(godapple)) {
				e.setCancelled(true);
				p.sendMessage(ChatColor.DARK_RED + "Enchanted Golden Apples are disabled");
			}
		}
	}
}
