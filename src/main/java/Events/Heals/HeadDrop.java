package main.java.Events.Heals;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import main.java.UHC;
import main.java.Recipes.RecipesList;

public class HeadDrop implements Listener {

	FileConfiguration config = UHC.getPlugin(UHC.class).getConfig();

	@EventHandler
	public void OnDeath(PlayerDeathEvent e) {

		Player p = e.getEntity().getPlayer();

		Location deathlocation = p.getLocation();

		ItemStack head = RecipesList.heads();

		if (config.getBoolean("Heads") == true) {
			if (e.getEntity().equals(p)) {
				deathlocation.getWorld().dropItemNaturally(deathlocation, head);
			}
		}

	}

	@EventHandler
	public void OnRightClick(PlayerInteractEvent e) {

		Player p = e.getPlayer();

		ItemStack head = RecipesList.heads();
		Action action = e.getAction();
		ItemStack item = e.getItem();

		if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
			if (item != null && item.getType() == Material.SKULL_ITEM
					&& item.getItemMeta().equals(RecipesList.heads().getItemMeta())) {
				if (config.getBoolean("Heads") == true) {
					if (p.getGameMode().equals(GameMode.SURVIVAL)) {
						p.getInventory().removeItem(head);
						e.setCancelled(true);
						p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 200, 0));
						p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 100, 0));
						p.sendMessage(ChatColor.GREEN + "You have eaten a head");
					} else {
						p.sendMessage(ChatColor.DARK_RED + "You must be in survival to eat heads!");
					}
				} else {
					p.sendMessage(ChatColor.DARK_RED + "Heads are disabled!");
					e.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void OnRightClickCorn(PlayerInteractEvent e) {

		Player p = e.getPlayer();

		ItemStack corn = new ItemStack(Material.GOLDEN_CARROT);
		ItemMeta meta = corn.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Cornucopia");
		corn.setItemMeta(meta);
		Action action = e.getAction();
		ItemStack item = e.getItem();

		if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
			if (item != null && item.getType() == Material.GOLDEN_CARROT
					&& item.getItemMeta().equals(RecipesList.corn().getItemMeta())) {
				if (p.getGameMode().equals(GameMode.SURVIVAL)) {
					p.getInventory().removeItem(corn);
					e.setCancelled(true);
					p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 200, 0));
					p.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, 1200, 0));
					p.sendMessage(ChatColor.GREEN + "You have consumed cornucopia");
				} else {
					p.sendMessage(ChatColor.DARK_RED + "You must be in survival to eat cornucopia!");
				}
			}
		}
	}

	@EventHandler
	public void OnRightClickGHead(PlayerInteractEvent e) {

		Player p = e.getPlayer();

		ItemStack ghead = RecipesList.ghead();
		ItemMeta gheadmeta = ghead.getItemMeta();
		Action action = e.getAction();
		ItemStack item = e.getItem();

		if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
			if (item != null && item.getType() == Material.SKULL_ITEM && item.getItemMeta().equals(gheadmeta)) {
				if (config.getBoolean("GoldenHeads") == true) {
					if (p.getGameMode().equals(GameMode.SURVIVAL)) {
						p.getInventory().removeItem(ghead);
						e.setCancelled(true);
						p.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 2400, 0));
						p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 200, 1));
						p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 100, 1));
						p.sendMessage(ChatColor.GREEN + "You have eaten a golden head");
					} else {
						p.sendMessage(ChatColor.DARK_RED + "You must be in survival to eat heads!");
					}
				} else {
					p.sendMessage(ChatColor.DARK_RED + "Heads are disabled!");
					e.setCancelled(true);
				}
			}
		}
	}
}
