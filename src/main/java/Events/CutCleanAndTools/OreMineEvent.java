package main.java.Events.CutCleanAndTools;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import main.java.UHC;

public class OreMineEvent implements Listener {

	FileConfiguration config = UHC.getPlugin(UHC.class).getConfig();

	@EventHandler
	public void OnBreak(BlockBreakEvent e) {
		Player p = e.getPlayer();
		Block block = e.getBlock();
		ItemStack ironb = new ItemStack(Material.IRON_INGOT);
		Location location = block.getLocation();
		ItemStack goldb = new ItemStack(Material.GOLD_INGOT);
		if (config.getBoolean("CutClean") == true) {
			if (e.getBlock().getType() == Material.IRON_ORE) {
				e.setCancelled(true);
				e.getBlock().setType(Material.AIR);
				location.getWorld().dropItemNaturally(location, ironb);
			}
			if (e.getBlock().getType() == Material.GOLD_ORE) {
				e.setCancelled(true);
				e.getBlock().setType(Material.AIR);
				location.getWorld().dropItemNaturally(location, goldb);
			}
		}
	}
}
