package main.java.Events.CutCleanAndTools;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.ItemStack;

import main.java.UHC;

public class PickaxeCraftEvent implements Listener {

	FileConfiguration config = UHC.getPlugin(UHC.class).getConfig();

	@EventHandler
	public void onCraft(CraftItemEvent e) {
		Player p = (Player) e.getWhoClicked();
		ItemStack stonepick = new ItemStack(Material.STONE_PICKAXE);
		ItemStack shovel = new ItemStack(Material.STONE_SPADE);
		ItemStack ironpick = new ItemStack(Material.IRON_PICKAXE);
		ItemStack ironshovel = new ItemStack(Material.IRON_SPADE);
		if (config.getBoolean("EnchantedStoneTools") == true) {
			if (e.getRecipe().getResult().equals(stonepick)) {
				e.getCurrentItem().addEnchantment(Enchantment.DIG_SPEED, 3);
			}
			if (e.getRecipe().getResult().equals(shovel)) {
				e.getCurrentItem().addEnchantment(Enchantment.DIG_SPEED, 3);
			}
			if (e.getRecipe().getResult().equals(ironpick)) {
				e.getCurrentItem().addEnchantment(Enchantment.DIG_SPEED, 2);
			}
			if (e.getRecipe().getResult().equals(ironshovel)) {
				e.getCurrentItem().addEnchantment(Enchantment.DIG_SPEED, 2);
			}
		}
	}
}
