package main.java.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import main.java.UHC;
import main.java.Recipes.RecipesList;

public class GiveCommand implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		FileConfiguration config = UHC.plugin.getConfig();

		if (command.getName().equalsIgnoreCase("uhcgive")) {

			if (sender.hasPermission("uhc.give")) {
				if (args.length == 1) {
					sender.sendMessage(ChatColor.RED + "Correct Usage: /uhcgive <player> <item>");
				} else if (args.length == 2) {
					Player p = Bukkit.getPlayerExact(args[0]);
					Inventory i = p.getInventory();
					switch (args[1]) {
					case "head":
						i.addItem(RecipesList.heads());
						p.sendMessage(ChatColor.GREEN + "You have recieved a Head");
						break;
					case "ghead":
						i.addItem(RecipesList.ghead());
						p.sendMessage(ChatColor.GREEN + "You have recieved a Golden Head");
						break;
					case "quickpick":
						i.addItem(RecipesList.QuickPick());
						p.sendMessage(ChatColor.GREEN + "You have recieved Quick Pick");
						break;
					case "sevenleague":
						i.addItem(RecipesList.SevenLeagueBoots());
						p.sendMessage(ChatColor.GREEN + "You have recieved Seven League Boots");
						break;
					case "bookofsharpening":
						i.addItem(RecipesList.bos());
						p.sendMessage(ChatColor.GREEN + "You have recieved a Book of Sharpening");
						break;
					case "bookofprotection":
						i.addItem(RecipesList.bop());
						p.sendMessage(ChatColor.GREEN + "You have recieved a Book of Protection");
						break;
					case "cupidsbow":
						i.addItem(RecipesList.CupidsBow());
						p.sendMessage(ChatColor.GREEN + "You have recieved Cupids Bow");
						break;
					case "dragonsword":
						i.addItem(RecipesList.DragonSword());
						p.sendMessage(ChatColor.GREEN + "You have recieved Dragon Sword");
						break;
					case "anduril":
						i.addItem(RecipesList.Anduril());
						p.sendMessage(ChatColor.GREEN + "You have recieved Anduril");
						break;
					case "dragonchestplate":
						i.addItem(RecipesList.dragonchestplate());
						p.sendMessage(ChatColor.GREEN + "You have recieved Dragon Chesplate");
						break;
					case "fusion":
						i.addItem(RecipesList.fusioncraftitem());
						p.sendMessage(ChatColor.GREEN + "You have recieved a fusion piece box");
						break;
					case "philos":
						i.addItem(RecipesList.philos());
						p.sendMessage(ChatColor.GREEN + "You have recieved Philosophers Pickaxe");
						break;
					case "exodus":
						i.addItem(RecipesList.exodus());
						p.sendMessage(ChatColor.GREEN + "You have recieved Exodus");
						break;
					case "tarnhelm":
						i.addItem(RecipesList.tarnhelm());
						p.sendMessage(ChatColor.GREEN + "You have recieved Tarnhelm");
						break;
					case "tabletofdestiny":
						i.addItem(RecipesList.tabletofdestiny());
						p.sendMessage(ChatColor.GREEN + "You have recieved Tablet of Destiny");
						break;
					case "spikedarmor":
						i.addItem(RecipesList.spikedarmor());
						p.sendMessage(ChatColor.GREEN + "You have recieved Spiked Armor");
						break;
					case "vidar":
						i.addItem(RecipesList.vidar());
						p.sendMessage(ChatColor.GREEN + "You have recieved Vidar");
						break;
					case "artemisbook":
						i.addItem(RecipesList.artbook());
						p.sendMessage(ChatColor.GREEN + "You have recieved Artemis Book");
						break;
					case "fenrir":
						i.addItem(RecipesList.fenrir());
						p.sendMessage(ChatColor.GREEN + "You have recieved Fenir");
						break;
					case "bookofthoth":
						i.addItem(RecipesList.bookofthoth());
						p.sendMessage(ChatColor.GREEN + "You have recieved Book Of Thoth");
						break;
					case "perun":
						i.addItem(RecipesList.axeofperun());
						p.sendMessage(ChatColor.GREEN + "You have recieved Axe of Perun");
						break;
					case "hide":
						i.addItem(RecipesList.hideofl());
						p.sendMessage(ChatColor.GREEN + "You have recieved Hide of Leviathan");
						break;
					case "nectar":
						i.addItem(RecipesList.nectar());
						p.sendMessage(ChatColor.GREEN + "You have recieved Nectar");
						break;
					case "potionoftoughness":
						i.addItem(RecipesList.potionoftoughness());
						p.sendMessage(ChatColor.GREEN + "You have recieved Potion Of Toughness");
						break;
					case "velocity":
						i.addItem(RecipesList.potionofvelocity());
						p.sendMessage(ChatColor.GREEN + "You have recieved Potion Of Velocity");
						break;
					case "holywater":
						i.addItem(RecipesList.holywater());
						p.sendMessage(ChatColor.GREEN + "You have recieved Holy Water");
						break;
					case "pancea":
						i.addItem(RecipesList.pancea());
						p.sendMessage(ChatColor.GREEN + "You have recieved Pancea");
						break;
					case "vorpalsword":
						i.addItem(RecipesList.VorpalSword());
						p.sendMessage(ChatColor.GREEN + "You have recieved Vorpal Sword");
						break;
					case "artemis":
						i.addItem(RecipesList.artmeis());
						p.sendMessage(ChatColor.GREEN + "You have recieved Artemis' Bow");
						break;
					case "excalibur":
						i.addItem(RecipesList.excalibur());
						p.sendMessage(ChatColor.GREEN + "You have recieved Excalibur");
						break;
					case "daredevil":
						i.addItem(RecipesList.daredevil());
						p.sendMessage(ChatColor.GREEN + "You have recieved Dare Devil");
						break;
					case "deathsythe":
						i.addItem(RecipesList.deathsythe());
						p.sendMessage(ChatColor.GREEN + "You have recieved Death's Sythe");
						break;
					case "hermesboots":
						i.addItem(RecipesList.hermesboots());
						p.sendMessage(ChatColor.GREEN + "You have recieved Hermes' Boots");
						break;
					case "barbarianchestplate":
						i.addItem(RecipesList.barbarian());
						p.sendMessage(ChatColor.GREEN + "You have recieved a Barbarian Chestplate");
						break;
					case "cornucopia":
						i.addItem(RecipesList.corn());
						p.sendMessage(ChatColor.GREEN + "You have recieved x5 Cornucopia");
						break;
					case "kingsrod":
						i.addItem(RecipesList.kingsrod());
						p.sendMessage(ChatColor.GREEN + "You have recieved a King's Rod");
						break;
					case "diceofgod":
						i.addItem(RecipesList.diceofgod());
						p.sendMessage(ChatColor.GREEN + "You have recieved a Dice Of God");
						break;
					default:
						sender.sendMessage(ChatColor.RED + "Correct Usage: /uhcgive <player> <item>");
						break;
					}
				} else {
					sender.sendMessage(ChatColor.RED + "Correct Usage: /uhcgive <player> <item>");
				}
			} else {
				sender.sendMessage(ChatColor.DARK_RED + "No Permission!");
			}
		}
		return false;
	}
}
