package main.java.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import main.java.UHC;

public class ConfigReload implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player p = (Player) sender;
		FileConfiguration config = UHC.plugin.getConfig();
		if (command.getName().equalsIgnoreCase("uhcreload")) {
			if (p.hasPermission("uhc.admin")) {
				UHC.plugin.reloadConfig();
				p.sendMessage(ChatColor.GREEN + "Config Reloaded");
			} else {
				sender.sendMessage(ChatColor.DARK_RED + "No Permission!");
			}
		}
		return false;
	}
}
