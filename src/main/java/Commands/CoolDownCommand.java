package main.java.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import main.java.UHC;
import main.java.Events.Items.ItemsEvents;
import net.md_5.bungee.api.ChatColor;

public class CoolDownCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		FileConfiguration config = UHC.getPlugin(UHC.class).getConfig();

		Player p = (Player) sender;

		if (command.getName().equalsIgnoreCase("itemcooldown")) {
			if (!(args.length == 1)) {
				if (args.length == 2) {
					switch (args[0]) {
					case "messages":
						if (p.hasPermission("uhc.cooldown.messages")) {
							switch (args[1]) {
							case "enable":
								if (!ItemsEvents.cooldownmessages.contains(p.getUniqueId())) {
									ItemsEvents.cooldownmessages.add(p.getUniqueId());
									p.sendMessage(
											ChatColor.GREEN + "Successful! You will now recieve cooldown messages");
								} else {
									p.sendMessage(ChatColor.RED + "Error! You already have cooldown messages enabled!");
								}
								break;
							case "disable":
								if (ItemsEvents.cooldownmessages.contains(p.getUniqueId())) {
									ItemsEvents.cooldownmessages.remove(p.getUniqueId());
									p.sendMessage(ChatColor.GREEN
											+ "Successful! You will now no longer recieve cooldown messages");
								} else {
									p.sendMessage(
											ChatColor.RED + "Error! You already have cooldown messages disabled!");
								}
								break;
							default:
								p.sendMessage(ChatColor.RED + "Correct Usage: /itemcooldown messages <enable/disable>");
								break;
							}
						} else {
							p.sendMessage(ChatColor.RED + "No Permission!");
						}
						break;
					case "perun":
						if (p.hasPermission("uhc.cooldown.item")) {
							if (Integer.valueOf(args[1]) != null) {
								config.set("peruncooldowntime", Integer.valueOf(args[1]));
								UHC.plugin.saveConfig();
								p.sendMessage(ChatColor.GREEN + "Successful! Cooldown time for Axe Of Perun set to "
										+ Integer.valueOf(args[1]) + " s.");
							} else {
								p.sendMessage(ChatColor.RED + "Correct Usage: /itemcooldown <item> <seconds>");
							}
						} else {
							p.sendMessage(ChatColor.RED + "No Permission!");
						}
						break;
					case "excalibur":
						if (p.hasPermission("uhc.cooldown.item")) {
							if (Integer.valueOf(args[1]) != null) {
								config.set("excaliburcooldowntime", Integer.valueOf(args[1]));
								UHC.plugin.saveConfig();
								p.sendMessage(ChatColor.GREEN + "Successful! Cooldown time for Excalibur set to "
										+ Integer.valueOf(args[1]) + " s.");
							} else {
								p.sendMessage(ChatColor.RED + "Correct Usage: /itemcooldown <item> <seconds>");
							}
						} else {
							p.sendMessage(ChatColor.RED + "No Permission!");
						}
						break;
					default:
						if (p.hasPermission("uhc.cooldown.messages")) {
							p.sendMessage(ChatColor.RED + "Correct Usage: /itemcooldown messages <enable/disable>");
						} else if (p.hasPermission("uhc.cooldown.change")) {
							p.sendMessage(ChatColor.RED + "Correct Usage: /itemcooldown <item> <seconds>");
						} else {
							p.sendMessage(ChatColor.RED + "No Permission!");
						}
					}
				}
			} else {
				if (p.hasPermission("uhc.cooldown.messages")) {
					p.sendMessage(ChatColor.RED + "Correct Usage: /itemcooldown messages <enable/disable>");
				} else if (p.hasPermission("uhc.cooldown.change")) {
					p.sendMessage(ChatColor.RED + "Correct Usage: /itemcooldown <item> <seconds>");
				} else {
					p.sendMessage(ChatColor.RED + "No Permission!");
				}
			}
		}
		return false;
	}

}
