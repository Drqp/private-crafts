package main.java.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import main.java.UHC;
import net.md_5.bungee.api.ChatColor;

public class ArrowMessageDisable implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		FileConfiguration config = UHC.plugin.getConfig();

		
		if(sender instanceof Player) {
			if(command.getName().equalsIgnoreCase("arrowmessages")) {
			
			Player p = (Player) sender;
			
			if(p.hasPermission("uhc.arrowtoggle")) {
				if(config.getBoolean("Arrow-Messages") == false) {
					config.set("Arrow-Messages", true);
					p.sendMessage(ChatColor.GREEN + "Arrow messages have been enabled!");
				} else {
					config.set("Arrow-Messages", false);
					p.sendMessage(ChatColor.GREEN + "Arrow messages have been disabled!");

				}
			}
		}
	}
		return false;
	}

}
