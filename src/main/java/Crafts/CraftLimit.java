package main.java.Crafts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import main.java.UHC;
import main.java.utils.AllItems;
import main.java.utils.CustomMeta;

public class CraftLimit {

	public static Table<UUID, ItemStack, Integer> nooftimescrafted = HashBasedTable.create();

	public static HashMap<ItemStack, Integer> maxcrafts = new HashMap<>();

	public static List<ItemStack> Items = new ArrayList<ItemStack>();

	public static void setNumberCrafted(Player p, ItemStack r, int i) {

		UUID uuid = p.getUniqueId();

		Map<ItemStack, Integer> craftedmap = nooftimescrafted.row(uuid);

		craftedmap.put(r, i);

	}

	public static int getNumberCrafted(Player p, ItemStack r) {

		UUID uuid = p.getUniqueId();

		Map<ItemStack, Integer> craftedmap = nooftimescrafted.row(uuid);

		int numberoftimes = craftedmap.get(r);

		return numberoftimes;

	}

	public static void addNumberCrafted(Player p, ItemStack r, int i) {

		UUID uuid = p.getUniqueId();

		Map<ItemStack, Integer> craftedmap = nooftimescrafted.row(uuid);

		int numberoftimes = craftedmap.get(r);

		craftedmap.put(r, numberoftimes + i);

	}

	public static void subtractNumberCrafted(Player p, ItemStack r, int i) {

		UUID uuid = p.getUniqueId();

		Map<ItemStack, Integer> craftedmap = nooftimescrafted.row(uuid);

		int numberoftimes = craftedmap.get(r);

		craftedmap.put(r, numberoftimes - i);

	}

	public static void setMaxCrafted(ItemStack r, int i) {

		maxcrafts.put(r, i);

	}

	public static int getMaxCrafts(ItemStack r) {

		return maxcrafts.get(r);

	}

	public static void registerMax() {

		FileConfiguration config = UHC.plugin.getConfig();

		for (ItemStack i : AllItems.Items) {

			try {
				String s = CustomMeta.getMetaUHC(i);

				if (CustomMeta.getMetaUHC(i) == "normal") {

					maxcrafts.put(i, config.getInt("Normal"));

				}

				if (CustomMeta.getMetaUHC(i) == "ultimate") {

					maxcrafts.put(i, config.getInt("Ultimate"));

				}

			} catch (NullPointerException e) {

				System.out.print("Not Registered: " + i + " due to an unexpected error!");
			}

		}

	}

	public static void registerPlayer(Player p) {

		UUID uuid = p.getUniqueId();
		System.out.print("debug");

		for (ItemStack i : AllItems.Items) {

			Map<ItemStack, Integer> craftedmap = nooftimescrafted.row(uuid);

			if (!craftedmap.containsKey(i)) {
				if (craftedmap.get(i) == null) {

					nooftimescrafted.put(uuid, i, 0);

				}
			}
		}
	}

}
