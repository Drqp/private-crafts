package main.java.Crafts;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

import main.java.utils.AllItems;
import net.md_5.bungee.api.ChatColor;

public class LimiterEvent implements Listener {

	@EventHandler
	public void onCraft(CraftItemEvent e) {

		if (AllItems.Items.contains(e.getRecipe().getResult())) {

			ItemStack r = e.getRecipe().getResult();

			Player p = (Player) e.getWhoClicked();

			if (!e.getClick().isShiftClick()) {

				if (CraftLimit.getNumberCrafted(p, r) >= CraftLimit.getMaxCrafts(r)) {

					e.setCancelled(true);

					p.sendMessage(ChatColor.RED + "You cannot craft this item! " + " (" + ChatColor.GREEN
							+ CraftLimit.getNumberCrafted(p, r) + "/" + CraftLimit.getMaxCrafts(r) + ChatColor.RED
							+ ")");

				} else {

					if (r.getItemMeta().getDisplayName() == null) {

						if (r.getType() == Material.IRON_INGOT) {

							p.sendMessage(ChatColor.YELLOW + "You have crafted Iron Pack" + ChatColor.YELLOW + " ("
									+ ChatColor.GREEN + (CraftLimit.getNumberCrafted(p, r) + 1) + "/"
									+ CraftLimit.getMaxCrafts(r) + ChatColor.YELLOW + ")");

							CraftLimit.addNumberCrafted(p, r, 1);

						}

						if (r.getType() == Material.GOLD_INGOT) {

							p.sendMessage(ChatColor.YELLOW + "You have crafted Gold Pack" + ChatColor.YELLOW + " ("
									+ ChatColor.GREEN + (CraftLimit.getNumberCrafted(p, r) + 1) + "/"
									+ CraftLimit.getMaxCrafts(r) + ChatColor.YELLOW + ")");

							CraftLimit.addNumberCrafted(p, r, 1);

						}

					} else {

						p.sendMessage(ChatColor.YELLOW + "You have crafted " + r.getItemMeta().getDisplayName()
								+ ChatColor.YELLOW + " (" + ChatColor.GREEN + (CraftLimit.getNumberCrafted(p, r) + 1)
								+ "/" + CraftLimit.getMaxCrafts(r) + ChatColor.YELLOW + ")");

						CraftLimit.addNumberCrafted(p, r, 1);

					}
				}

			} else {
				e.setCancelled(true);
				p.sendMessage(ChatColor.RED + "Don't Shift Click!");
			}

		}
	}

	@EventHandler

	public void onJoin(PlayerJoinEvent e) {

		Player p = e.getPlayer();

		CraftLimit.registerPlayer(p);

	}
}
